package com.gadgeon.registerSensor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gadgeon.dbManager.dbOperation;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.sensormetadata;

public class LsmRegister implements Runnable {
	sensormetadata da;
	loadDataMap mp = new loadDataMap();
	Connection con = null;
	static Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();
	public boolean registerstatus = false;
	public boolean createstatus = false;
	final static Logger logger = Logger.getLogger(LsmRegister.class);

	public LsmRegister() {

	}

	public LsmRegister(String userid, String sensorname, String sensortype,
			String sensorinfo, String latitude, String longitude, String tmax,
			String tmin, String field, String unit, String location,
			String enddevice) {
		da = new sensormetadata();
		da.setUserid(userid);
		da.setSensorid(sensorname);
		da.setSensorname(sensorname);
		da.setSensortype(sensortype);
		da.setSensorinfo(sensorinfo);
		da.setSensorlati(latitude);
		da.setSensorlongi(longitude);
		da.setSensormax(Double.parseDouble(tmax));
		da.setSensormin(Double.parseDouble(tmin));
		da.setField(field);
		da.setSensorunit(unit);
		da.setLocation(location);
		da.setSensorstatus("normal");
		Date d = new Date();
		String lastuptime = d.toString();
		da.setLastuptime(lastuptime);
		da.setEnddevice(enddevice);
		registerstatus = false;
		createstatus = false;
	}

	public void run() {

		try {

			createSensorDataFolder();
			createmetadatafile(da.getSensorname(), da.getSensortype(),
					da.getSensorinfo(), da.getSensorlati(), da.getSensorlati(),
					da.getSensormax(), da.getSensormin(), da.getField(),
					da.getSensorunit());
			createxmlfile(da.getSensorname(), da.getSensortype(),
					da.getSensorinfo(), da.getSensorlati(), da.getSensorlati(),
					da.getSensormax(), da.getSensormin(), da.getField(),
					da.getSensorunit());
			registerSensor(da.getSensorname());

			if ((registerstatus == true) && (createstatus == true)) {
				dbOperation dbop = new dbOperation();
				dbop.sensorRegister(da);
				sensormap = mp.getSensormap();
				sensormap.put(da.getSensorname(), da);
				registerstatus = false;
				createstatus = false;
			}else
			{
				throw new RuntimeException();
			}

		} catch (Exception e) {
			registerstatus = false;
			createstatus = false;
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			
		}
	}

	private void createmetadatafile(String sensorname, String sensortype,
			String sensorinfo, String latitude, String longitude, Double tmax,
			Double tmin, String field, String unit)
			throws FileNotFoundException, UnsupportedEncodingException {
		logger.info(loggerdateFormat.format(loggerdate)
				+ ": Control on method --> createmetadatafile\n");
		PrintWriter writer = new PrintWriter("/tmp/sensorData/" + sensorname
				+ ".metadata", "UTF-8");
		writer.println("sensorID=\"http://lsm.deri.ie/resource/61330620147099\"");
		writer.println("sensorName=" + sensorname);
		writer.println("source=\"http://gadgeon.com:22002/gsn?REQUEST=113&name=gadgeon_sensor\"");
		writer.println("sourceType=" + sensortype);
		writer.println("sensorType=" + sensorname);
		writer.println("information=" + sensorinfo);
		writer.println("author=GadgeonInc");
		writer.println("feature=\"http://gadgeon.com/OpenIoT/opensensefeature\"");
		writer.println("fields=\"" + field + "\"");
		writer.println("field." + field
				+ ".propertyName=\"http://gadgeon.com/OpenIoT/" + field + "\"");
		writer.println("field." + field + ".unit=" + unit);
		writer.println("latitude=" + latitude);
		writer.println("longitude=" + longitude);
		writer.println("maximumThreshold=" + tmax);
		writer.println("minimumThreshold=" + tmin);
		writer.close();
	}

	private void createxmlfile(String sensorname, String sensortype,
			String sensorinfo, String latitude, String longitude, Double tmax,
			Double tmin, String field, String unit)
			throws FileNotFoundException, UnsupportedEncodingException {
		logger.info(loggerdateFormat.format(loggerdate)
				+ ": Control on method --> createxmlfile\n");
		PrintWriter writer = new PrintWriter("/tmp/sensorData/" + sensorname
				+ ".xml", "UTF-8");
		writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.println("<virtual-sensor name=\"" + sensorname
				+ "\" priority=\"10\" >");
		writer.println("<processing-class>");
		writer.println("<class-name>org.openiot.gsn.vsensor.LSMExporter</class-name>");
		writer.println("<init-params>");
		writer.println("<param name=\"allow-nulls\">false</param>");
		writer.println("<param name=\"publish-to-lsm\">true</param>");
		writer.println("</init-params>");
		writer.println("<output-structure>");
		writer.println("<field name=\"" + field + "\" type=\"double\" />");
		writer.println("</output-structure>");
		writer.println("</processing-class>");
		writer.println("<description>" + sensorinfo + "</description>");
		writer.println("<life-cycle pool-size=\"20\"/>");
		writer.println("<addressing>");
		writer.println("</addressing>");
		writer.println("<streams>");
		writer.println("<stream name=\"input1\">");
		writer.println("<source alias=\"source1\" sampling-rate=\"1\" storage-size=\"1\">");
		writer.println("<address wrapper=\"gadgeonrest\">");
		writer.println("<predicate key=\"fields\">timed, " + field
				+ "</predicate>");
		writer.println("<predicate key=\"formats\">timestamp, numeric</predicate>");
		writer.println("<predicate key=\"timezone\">Etc/UTC</predicate>");
		writer.println("</address>");
		writer.println("<query>select * from wrapper");
		writer.println("</query>");
		writer.println("</source>");
		writer.println("<query>select " + field
				+ " , timed from source1</query>");
		writer.println("</stream>");
		writer.println("</streams>");
		writer.println("</virtual-sensor>");
		writer.close();
	}

	private void registerSensor(String sensorname) throws IOException,
			InterruptedException {

		create(sensorname);
		register(sensorname);

	}

	public void test() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> test----------------------------------kk\n");
			String strUsrName = "waterbit";
			String strPsswd = "waterbit";
			URL jsonpage = new URL(
					"http://52.0.88.108:9080/GadgeonREST/rest/services/"
							+ strUsrName + "/" + strPsswd + "/userlogin");
			URLConnection urlcon = jsonpage.openConnection();
			// urlcon.setConnectTimeout(5000);
			BufferedReader buffread = new BufferedReader(new InputStreamReader(
					urlcon.getInputStream()));
			String strTemp = buffread.readLine();
			System.out.println(strTemp);
		} catch (Exception ex) {
			registerstatus = false;
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ ex.getMessage() + " \n");
		}
	}

	private void create(String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> create\n");
			String urlToConnect = "http://52.0.88.104:22001/vs/vsensor/"
					+ sensorname + "/create";
			File fileToUpload = new File("/tmp/sensorData/" + sensorname
					+ ".xml");
			String boundary = Long.toHexString(System.currentTimeMillis());
			URLConnection connection = new URL(urlToConnect).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + boundary);
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new OutputStreamWriter(
						connection.getOutputStream(), "UTF-8"));
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(fileToUpload), "UTF-8"));
					for (String line; (line = reader.readLine()) != null;) {
						writer.println(line);
					}
				} finally {
					if (reader != null)
						try {
							reader.close();
						} catch (IOException logOrIgnore) {
							logger.error(loggerdateFormat.format(loggerdate)
									+ ": " + logOrIgnore.getMessage() + " \n");
						}
				}
			} finally {
				if (writer != null)
					writer.close();
			}
			int responseCode = ((HttpURLConnection) connection)
					.getResponseCode();
			if (responseCode == 200) {
				createstatus = true;
			} else {
				createstatus = false;
			}
		} catch (Exception ex) {
			createstatus = false;
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ ex.getMessage() + " \n");
		}

	}

	private void register(String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> register\n");
			String urlToConnect = "http://52.0.88.104:22001/vs/vsensor/"
					+ sensorname + "/register";
			File fileToUpload = new File("/tmp/sensorData/" + sensorname
					+ ".metadata");
			String boundary = Long.toHexString(System.currentTimeMillis());
			URLConnection connection = new URL(urlToConnect).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + boundary);
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new OutputStreamWriter(
						connection.getOutputStream(), "UTF-8"));
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(fileToUpload), "UTF-8"));
					for (String line; (line = reader.readLine()) != null;) {
						writer.println(line);
					}
				} finally {
					if (reader != null)
						try {
							reader.close();
						} catch (IOException logOrIgnore) {
							logger.error(loggerdateFormat.format(loggerdate)
									+ ": " + logOrIgnore.getMessage() + " \n");
						}
				}
			} finally {
				if (writer != null)
					writer.close();
			}
			int responseCode = ((HttpURLConnection) connection)
					.getResponseCode();
			if (responseCode == 200) {
				registerstatus = true;
			} else {
				registerstatus = false;
			}
		} catch (Exception ex) {
			registerstatus = false;
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ ex.getMessage() + " \n");
		}

	}

	private void createSensorDataFolder() {
		logger.info(loggerdateFormat.format(loggerdate)
				+ ": Control on method --> createSensorDataFolder\n");
		File file = new File("/tmp/sensorData");
		if (!file.exists()) {
			if (file.mkdir()) {
			} else {
			}
		}
	}

}
