package com.gadgeon.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.internal.wire.MqttPublish;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.gadgeon.dataRetrieval.retrievalManager;
import com.gadgeon.dbManager.EndPointDevice;
import com.gadgeon.dbManager.GraphProfile;
import com.gadgeon.dbManager.LiveUpdate;
import com.gadgeon.dbManager.SensorData;
import com.gadgeon.dbManager.SensorGraph;
import com.gadgeon.dbManager.SensorListByEndDevice;
import com.gadgeon.dbManager.UserList;
import com.gadgeon.dbManager.dbOperation;
import com.gadgeon.dbManager.sensorList;
import com.gadgeon.geolocation.GeoFinder;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.mosquitto.Publish;
import com.gadgeon.mosquitto.Subscribe;
import com.gadgeon.registerSensor.LsmRegister;
import com.gadgeon.sensor.SensorDelete;
import com.gadgeon.sensor.sensormetadata;
import com.gadgeon.timer.LastUpTime;
import com.gadgeon.waterbitMQTT.MqttPublishTimer;
import com.gadgeon.waterbitMQTT.MqttSubscribe;
import com.gadgeon.waterbitMQTT.PublishMqtt;
import com.gadgeon.waterbitMQTT.StatusPublishTimer;
import com.gadgeon.waterbitMQTT.StatusSubscribe;

@Path("/services")
public class GadgeonRestServer {

	public static int testid = 0;
	public retrievalManager rm = new retrievalManager();
	public dbOperation dbo = new dbOperation();
	public loadDataMap map;
	public Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
	public loadDataMap mp = new loadDataMap();
	final static Logger logger = Logger.getLogger(GadgeonRestServer.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();
	public LastUpTime lastuptime = new LastUpTime();
	public MqttPublishTimer mqttpublish = new MqttPublishTimer();
	public StatusPublishTimer statusPublish = new StatusPublishTimer();

	/**
	 * Function welcomeMessage is used to display the API list.
	 * 
	 * @return String : Return a string conatin api list.
	 * @throws Exception
	 */

	@GET()
	@Produces("text/plain")
	public String welcomeMessage() {
		logger.info(loggerdateFormat.format(loggerdate)
				+ ": Control on method --> welcomeMessage\n");
		String welcomeText;

		welcomeText = "Welcome to Gadgeon Rest API Interface For VitalHerd Project\n"
				+ "=====================================\n\n"
				+ "This Interface provides the folowing API Services:\n"

				+ "1 . \n"
				+ "		Reset : /reset\n"
				+ "		Produces : text/plain\n"

				+ "2 . \n"
				+ "		User Login : username/password/userlogin\n"
				+ "		Produces : text/plain\n"

				+ "3 . \n"
				+ "		User Register : username/password/userInfo/email/phone/userregister\n"
				+ "		Produces : text/plain\n"

				+ "4 . \n"
				+ "		Change Password : userid/oldpassword/newpassword/changepassword\n"
				+ "		Produces : text/plain\n"

				+ "5 . \n"
				+ "		Get UserList : /getUserList\n"
				+ "		Produces : application/xml\n"

				+ "6 . \n"
				+ "		Update User : userid/userinfo/mail/phone/updateuser\n"
				+ "		Produces : text/plain\n"

				+ "7 . \n"
				+ "		Delete User : userid/deleteuser\n"
				+ "		Produces : text/plain\n"

				+ "8 . \n"
				+ "		Sensor Register : userid/sensorname/sensortype/sensorinfo/latitude/longitude/tmax/tmin/field/unit/location/profile/register\n"
				+ "		Produces : text/plain\n"

				+ "9 . \n"
				+ "		Get SensorList : /getSensorList\n"
				+ "		Produces : application/xml\n"

				+ "10 . \n"
				+ "		Get SensorList By UserId : userid/getSensorListByUserId\n"
				+ "		Produces : application/xml\n"

				+ "11 . \n"
				+ "		Clear Alarm : sensorname/clearalarm\n"
				+ "		Produces : text/plain\n"

				+ "12 . \n"
				+ "		Add Sensordata : sensorname/currentvalue/addsensordata\n"
				+ "		Produces : text/plain\n"

				+ "13 . \n"
				+ "		Mqtt Push : sensorname/mac/command/mqtt\n"
				+ "		Produces : text/plain\n"

				+ "14 . \n"
				+ "		Update Sensor : sensorname/info/latitude/longitude/location/maxvalue/minvalue/profile/updatesensor\n"
				+ "		Produces : text/plain\n"

				+ "15 . \n"
				+ "		Delete Sensor : sensorname/deletesensor\n"
				+ "		Produces : text/plain\n"

				+ "16 . \n"
				+ "		Get Sensor Values : sensorname/getsensorvalues\n"
				+ "		Produces : text/plain\n"

				+ "17 . \n"
				+ "		Get Live Data : sensorname/getlivedata\n"
				+ "		Produces : text/plain\n"

				+ "18 . \n"
				+ "		Get Alarm Count : userid/sensorname/getalarmcount\n"
				+ "		Produces : text/plain\n"

				+ "19 . \n"
				+ "		Add Sensor Type : userid/sensortypename/addsensortype\n"
				+ "		Produces : text/plain\n"

				+ "20 . \n"
				+ "		Delete Sensor Type : sensortype/deletesensortype\n"
				+ "		Produces : text/plain\n"

				+ "21 . \n"
				+ "		Get SensorType : /getSensorType\n"
				+ "		Produces : application/xml\n"

				+ "22 . \n"
				+ "		Add Graph Profile : userid/graphprofile/sensortype/xlabel/ylabel/min/max/addgraphprofile\n"
				+ "		Produces : text/plain\n"

				+ "23 . \n"
				+ "		Delete Graph Profile : userid/profilename/deletegraphprofile\n"
				+ "		Produces : text/plain\n"

				+ "24 . \n"
				+ "		Update Graph Profile :profile/xlabel/ylabel/min/max/updategraphprofile\n"
				+ "		Produces : text/plain\n"

				+ "25 . \n"
				+ "		Get ProfileList : /getProfileList\n"
				+ "		Produces : application/xml\n"

				+ "26 . \n"
				+ "		Get Profile By Type : sensortype/GetProfileByType\n"
				+ "		Produces : application/xml\n"

				+ "27 . \n"
				+ "		Get Profile By Name : profilename/GetProfileByName\n"
				+ "		Produces : application/xml\n"

				+ "28 . \n"
				+ "		Get SensorGraph By sensor : sensorname/GetSensorGraphBysensor\n"
				+ "		Produces : application/xml\n"

				+ "29 . \n"
				+ "		Get SensorList By Name : userid/sensorname/getSensorListByName\n"
				+ "		Produces : application/xml\n"

				+ "=====================================\n";

		logger.info(loggerdateFormat.format(loggerdate) + welcomeText);

		return welcomeText;
	}

	/**
	 * constructor is used to retrive all data of sensors from mysql db to
	 * memory.
	 * 
	 * @return String : Return a string conatin api list.
	 * @throws Exception
	 */

	public GadgeonRestServer() throws SQLException, IOException {
		try {
			if (testid == 0) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": Control on constructor --> GadgeonRestServer\n");
				mp.loadMap();
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": initial data loading from db to memmory is completed \n");
				try {
					MqttSubscribe subscribe = new MqttSubscribe();
					StatusSubscribe statusSubscribe=new StatusSubscribe();
					subscribe.startSubscribe();//for periodic device data
					statusSubscribe.startSubscribe();//for periodic device status
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": mqtt subscription started \n");
				} catch (Exception e) {
				}
				lastuptime.startcheck();
				mqttpublish.startcheck();//for periodic device data
				statusPublish.startcheck();//for periodic device status
				
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": last uptime timer started \n");
				testid++;

			}
			sensormap = mp.getSensormap();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor details from memory \n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		}
	}

	/**
	 * function reset is used to retrieve all data of sensors from mysql db to
	 * memory.
	 * 
	 * @return Response : without error,Return OK. else return error message.
	 * @throws Exception
	 */
	@GET
	@Path("/reset")
	@Produces("text/plain")
	public Response reset() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> reset\n");
			mp.loadMap();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": data loading from db to memmory is completed \n");
			sensormap = mp.getSensormap();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor details from memory \n");
			return Response.status(200).entity("ok").build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function userRegister is used to register a new user.
	 * 
	 * @param username
	 *            : A string data containing user name.
	 * @param password
	 *            : A string data containing password.
	 * @param userInfo
	 *            : A string data containing user informations.
	 * @param email
	 *            : A string data containing email id of user.
	 * @param phone
	 *            : A string data containing phone number of user.
	 * @return Response : without error Return OK else return NOK.
	 * @throws Exception
	 */
	@GET
	@Path("{username}/{password}/{userInfo}/{email}/{phone}/userregister")
	@Produces("text/plain")
	public Response userRegister(@PathParam("username") String username,
			@PathParam("password") String password,
			@PathParam("userInfo") String userInfo,
			@PathParam("email") String email, @PathParam("phone") String phone) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> userRegister\n");
			dbo.userRegister(username, password, userInfo, email, phone);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user registration completed successfully\n");
			return Response.status(200).entity("ok").build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function userLogin is used to login user with username and password.
	 * 
	 * @param username
	 *            : A string data containing username to check
	 * @param password
	 *            : A string data containing password to check
	 * @return userid : A string value returning the userid of the curresponding
	 *         user
	 * @throws Exception
	 */
	@GET
	@Path("{username}/{password}/userlogin")
	@Produces("text/plain")
	public Response userLogin(@PathParam("username") String username,
			@PathParam("password") String password) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> userLogin\n");
			return Response.status(200)
					.entity(dbo.userLogin(username, password)).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("0").build();
		}
	}

	/**
	 * Function changePassword is used to change password of a selected user.
	 * 
	 * @param userid
	 *            : A string data containing user Id of selected user.
	 * @param oldpassword
	 *            : A string data containing current password of user.
	 * @param newpassword
	 *            : A string data containing new password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/{oldpassword}/{newpassword}/changepassword")
	@Produces("text/plain")
	public Response changePassword(@PathParam("userid") String userid,
			@PathParam("oldpassword") String oldpassword,
			@PathParam("newpassword") String newpassword) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> changePassword\n");
			int sta = dbo.changePassword(userid, oldpassword, newpassword);
			if (sta == 1) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": user password changed successfully\n");
				return Response.status(200).entity("ok").build();
			} else {
				return Response.status(200).entity("0").build();
			}

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee(Document doc, String userid,
			String username, String userInfo, String email, String phone) {
		Element employee = doc.createElement("user");
		employee.appendChild(getUserElements(doc, employee, "userid", userid));
		employee.appendChild(getUserElements(doc, employee, "username",
				username));
		employee.appendChild(getUserElements(doc, employee, "userInfo",
				userInfo));
		employee.appendChild(getUserElements(doc, employee, "email", email));
		employee.appendChild(getUserElements(doc, employee, "phone", phone));
		return employee;
	}

	/**
	 * Function getUserList is used to retrieve user details.
	 * 
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("/getUserList")
	@Produces("application/xml")
	public Response getUserList() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getUserList\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<UserList> mylist = dbo.getUserList();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading userlist from db is completed\n");
			for (UserList m : mylist) {
				rootElement.appendChild(getEmployee(doc, m.getUserid(),
						m.getUsername(), m.getUserInfo(), m.getEmail(),
						m.getPhone()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function clearalarm is used to clear alarm of a selected sensor.
	 * 
	 * @param sensorname
	 *            : A string data containing user Id of selected user.
	 * @param sensorname
	 *            : A string data containing current password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/{userinfo}/{mail}/{phone}/updateuser")
	public Response updateUser(@PathParam("userid") String userid,
			@PathParam("userinfo") String userinfo,
			@PathParam("mail") String mail, @PathParam("phone") String phone) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateUser\n");
			dbo.updateUser(Integer.parseInt(userid), userinfo, mail, phone);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user details updated successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function changePassword is used to change password of a selected user.
	 * 
	 * @param userid
	 *            : A string data containing user Id of selected user.
	 * @param oldpassword
	 *            : A string data containing current password of user.
	 * @param newpassword
	 *            : A string data containing new password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/deleteuser")
	@Produces("text/plain")
	public Response deleteUser(@PathParam("userid") String userid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteUser\n");
			dbo.deleteUser(Integer.parseInt(userid));
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user deleted successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/********************************************************/

	/*** Sensor operations starts here*********************** */

	/********************************************************/

	/**
	 * Function changePassword is used to change password of a selected user.
	 * 
	 * @param userid
	 *            : A string data containing user Id of selected user.
	 * @param oldpassword
	 *            : A string data containing current password of user.
	 * @param newpassword
	 *            : A string data containing new password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/{enddevice}/{sensorname}/{sensortype}/{sensorinfo}/{latitude}/{longitude}/{tmax}/{tmin}/{field}/{unit}/{location}/{profile}/register")
	@Produces("text/plain")
	public Response registerSensor(@PathParam("userid") String userid,
			@PathParam("enddevice") String enddevice,
			@PathParam("sensorname") String sensorname,
			@PathParam("sensortype") String sensortype,
			@PathParam("sensorinfo") String sensorinfo,
			@PathParam("latitude") String latitude,
			@PathParam("longitude") String longitude,
			@PathParam("tmax") String tmax, @PathParam("tmin") String tmin,
			@PathParam("field") String field, @PathParam("unit") String unit,
			@PathParam("location") String location,
			@PathParam("profile") String profile) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> registerSensor\n");
			enddevice = URLDecoder.decode(enddevice, "UTF-8").toLowerCase();
			sensorname = URLDecoder.decode(sensorname, "UTF-8").toLowerCase();
			location = URLDecoder.decode(location, "UTF-8");
			sensorinfo = URLDecoder.decode(sensorinfo, "UTF-8");

			if (location == "NA") {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": Control on NA location\n");
				GeoFinder g = new GeoFinder();
				JSONObject ret2 = g.getLocationInfo(latitude, longitude);
				JSONObject location1;
				String location_string1;
				location1 = ret2.getJSONArray("results").getJSONObject(0);
				location_string1 = location1.getString("formatted_address");
				String addrs[] = location_string1.split(",");
				location = addrs[1];
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": sensor location generated successfully \n");
			}

			sensormetadata value = sensormap.get(sensorname);
			if (value == null) {

				LsmRegister lsm = new LsmRegister(userid, sensorname,
						sensortype, sensorinfo, latitude, longitude, tmax,
						tmin, field, unit, location, enddevice);
				Thread t = new Thread(lsm);
				t.start();

				logger.info(loggerdateFormat.format(loggerdate)
						+ ": sensor registration thread started successfully \n");
				if (profile == "NA") {
					dbo.addSensorGraphProfile(sensorname, "default");
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": default profile with sensor attached successfully \n");
				} else if (sensortype == "relay") {
					dbo.addSensorGraphProfile(sensorname, "NA");
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": NA profile with sensor attached successfully \n");
				} else {
					dbo.addSensorGraphProfile(sensorname, profile);
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": normal profile with sensor attached successfully \n");
				}
				return Response.status(200).entity("ok").build();
			} else {
				if (!(value.getSensorlati().equals(latitude) || value
						.getSensorlongi().equals(longitude))) {
					GeoFinder g = new GeoFinder();
					JSONObject ret2 = g.getLocationInfo(latitude, longitude);
					JSONObject location1;
					String location_string1;
					location1 = ret2.getJSONArray("results").getJSONObject(0);
					location_string1 = location1.getString("formatted_address");
					String addrs[] = location_string1.split(",");
					location = addrs[1];
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": sensor location generated successfully \n");
					value.setLocation(location);
					value.setSensorlati(latitude);
					value.setSensorlongi(longitude);
					dbo.editSensor(value);
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": sensor location updated successfully \n");
				}
				return Response.status(200).entity("nok").build();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements1(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee1(Document doc, String sensorname,
			String sensortype, String sensorinfo, String alarmcount,
			String status, String lastuptime) {
		Element employee = doc.createElement("sensor");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements1(doc, employee, "sensorname",
				sensorname));
		employee.appendChild(getUserElements1(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements1(doc, employee, "sensorinfo",
				sensorinfo));
		employee.appendChild(getUserElements1(doc, employee, "alarmcount",
				alarmcount));
		employee.appendChild(getUserElements1(doc, employee, "status", status));
		employee.appendChild(getUserElements1(doc, employee, "lastuptime",
				lastuptime));
		return employee;
	}

	/**
	 * Function getUserList is used to retrieve user details.
	 * 
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("/getSensorList")
	@Produces("application/xml")
	public Response getSensorList() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorList\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/sensor", "OpenIOT");
			doc.appendChild(rootElement);
			HashMap<String, Date> StatusMap = Subscribe.StatusMap;
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading live device list from mqtt completed\n");
			ArrayList<sensorList> mylist = new ArrayList<sensorList>();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor list from memory\n");
			for (Map.Entry<String, sensormetadata> entry : sensormap.entrySet()) {
				mylist.add(new sensorList(entry.getValue().getSensorid(), entry
						.getValue().getSensortype(), entry.getValue()
						.getLocation(), entry.getValue().getSensoralarmcnt(),
						entry.getValue().getLastuptime(), entry.getValue()
								.getSensorstatus(), entry.getValue()
								.getSensorlati(), entry.getValue()
								.getSensorlongi()));
			}

			for (sensorList m : mylist) {
				Date value = StatusMap.get(m.getSensorid().toString());
				String status = "0";
				if (value != null) {
					status = "1";
				} else {
					status = "0";
				}

				rootElement.appendChild(getEmployee1(doc, m.getSensorid(),
						m.getSensortype(), m.getSensorlocation(),
						Double.toString(m.getSensoralarmcnt()), status,
						m.getLastuptime()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements2(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee2(Document doc, String sensorname,
			String sensortype, String sensorinfo, String alarmcount,
			String status, String lastuptime, String sensorstatus,
			String latitude, String longitude) {
		Element employee = doc.createElement("sensor");
		employee.appendChild(getUserElements2(doc, employee, "sensorname",
				sensorname));
		employee.appendChild(getUserElements2(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements2(doc, employee, "sensorinfo",
				sensorinfo));
		employee.appendChild(getUserElements2(doc, employee, "alarmcount",
				alarmcount));
		employee.appendChild(getUserElements2(doc, employee, "status", status));
		employee.appendChild(getUserElements1(doc, employee, "lastuptime",
				lastuptime));
		employee.appendChild(getUserElements1(doc, employee, "alarmstatus",
				sensorstatus));
		employee.appendChild(getUserElements1(doc, employee, "latitude",
				latitude));
		employee.appendChild(getUserElements1(doc, employee, "longitude",
				longitude));
		return employee;
	}

	/**
	 * Function getUserList is used to retrieve user details.
	 * 
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/getSensorListByUserId")
	@Produces("application/xml")
	public Response getSensorListByUserId(@PathParam("userid") String userid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorListByUserId\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/sensor", "OpenIOT");
			doc.appendChild(rootElement);
			 HashMap<String, Date> StatusMap = StatusSubscribe.getStatus();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading live device list from mqtt completed\n");
			ArrayList<sensorList> mylist = new ArrayList<sensorList>();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor list from memory\n");
			for (Map.Entry<String, sensormetadata> entry : sensormap.entrySet()) {
				String id = entry.getValue().getUserid();
				if (userid.equals(id)) {
					mylist.add(new sensorList(entry.getValue().getSensorid(),
							entry.getValue().getSensortype(), entry.getValue()
									.getLocation(), entry.getValue()
									.getSensoralarmcnt(), entry.getValue()
									.getLastuptime(), entry.getValue()
									.getSensorstatus(), entry.getValue()
									.getSensorlati(), entry.getValue()
									.getSensorlongi()));
				}
			}

			Collections.sort(mylist, new Comparator<sensorList>() {
				public int compare(sensorList list, sensorList anotherlist) {
					return list.getSensorid().compareToIgnoreCase(
							anotherlist.getSensorid());
				}
			});

			for (sensorList m : mylist) {
				 Date value = StatusMap.get(m.getSensorid().toString());
				String status = "0";
				
				 if (value != null) { status = "1";
				  
				  } else { status = "0"; }
				 
				rootElement.appendChild(getEmployee2(doc, m.getSensorid(),
						m.getSensortype(), m.getSensorlocation(),
						Double.toString(m.getSensoralarmcnt()), status,
						m.getLastuptime(), m.getSensorstatus(),
						m.getLatitude(), m.getLongitude()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function clear alarm is used to clear alarm of a selected sensor.
	 * 
	 * @param sensorname
	 *            : A string data containing user Id of selected user.
	 * @param sensorname
	 *            : A string data containing current password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{sensorname}/clearalarm")
	public Response ClearAlarm(@PathParam("sensorname") String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> ClearAlarm\n");
			dbOperation dbo = new dbOperation();
			sensormetadata ds = sensormap.get(sensorname);
			ds.setSensoralarmcnt(0);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm cleared from memory successfully\n");
			dbo.clearAlarm(sensorname);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm cleared from db successfully\n");

			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	/**
	 * Function clearalarm is used to clear alarm of a selected sensor.
	 * 
	 * @param sensorname
	 *            : A string data containing user Id of selected user.
	 * @param sensorname
	 *            : A string data containing current password of user.
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{sensorname}/{reporttime}/{currentvalue}/addsensordata")
	public Response addSensorData(@PathParam("sensorname") String sensorname,
			@PathParam("currentvalue") String currentvalue,
			@PathParam("reporttime") String reporttime) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addSensorData\n");
			reporttime = URLDecoder.decode(reporttime, "UTF-8");
			sensormetadata metadata = sensormap.get(sensorname);
			metadata.setLastuptime(reporttime);
			Thread t = new Thread(new LiveUpdate(sensorname,
					Double.parseDouble(currentvalue)));
			t.start();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": LiveUpdating thread started successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("{sensorname}/{mac}/{command}/mqtt")
	public Response mqttPublish(@PathParam("sensorname") String sensorname,
			@PathParam("mac") String mac, @PathParam("command") String command) {
		String output = "";
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> mqttPublish\n");
			command = URLDecoder.decode(command, "UTF-8");
			Publish pb = new Publish();
			output = pb.send(sensorname, mac, command);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": command published to mqtt \n");
			return Response.status(200).entity(output).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity(e.getMessage()).build();
		}

	}

	@GET
	@Path("{sensorname}/{info}/{latitude}/{longitude}/{location}/{maxvalue}/{minvalue}/{profile}/updatesensor")
	public Response updateSensor(@PathParam("sensorname") String sensorname,
			@PathParam("info") String info,
			@PathParam("latitude") String latitude,
			@PathParam("longitude") String longitude,
			@PathParam("location") String location,
			@PathParam("maxvalue") String maxvalue,
			@PathParam("minvalue") String minvalue,
			@PathParam("profile") String profile) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateSensor\n");

			info = URLDecoder.decode(info, "UTF-8");
			location = URLDecoder.decode(location, "UTF-8");
			profile = URLDecoder.decode(profile, "UTF-8");

			sensormetadata md = new sensormetadata();
			md = sensormap.get(sensorname);

			md.setSensorinfo(info);
			md.setSensorlati(latitude);
			md.setSensorlongi(longitude);
			md.setLocation(location);
			md.setSensormin(Double.parseDouble(minvalue));
			md.setSensormax(Double.parseDouble(maxvalue));
			dbo.editSensor(md);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Sensor details updated successfully\n");
			dbo.editSensorProfile(sensorname, profile);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Sensor graph details updated successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}

	}

	@GET
	@Path("{sensorname}/deletesensor")
	public Response deleteSensor(@PathParam("sensorname") String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteSensor\n");
			Thread t = new Thread(new SensorDelete(sensorname));
			t.start();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Sensor deleting thread started successfully\n");
			sensormap.remove(sensorname);
			dbo.deleteSensorProfile(sensorname);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Sensor profile deleted successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}

	}

	@GET
	@Path("{sensorname}/getsensorvalues")
	public Response getsensorvalues(@PathParam("sensorname") String sensorname) {
		String ret = "";
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getsensorvalues\n");
			sensormetadata md2 = new sensormetadata();
			md2 = sensormap.get(sensorname);
			Double min = md2.getSensormin();
			Double max = md2.getSensormax();
			ret = "min=" + min.toString() + ",max=" + max.toString();
			return Response.status(200).entity(ret).build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}

	}

	/*
	 * this is the method for get current value
	 */
	@GET
	@Path("{sensorname}/getlivedata")
	@Produces("text/plain")
	public String GetCurrentValue(@PathParam("sensorname") String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetCurrentValue\n");
			return rm.getCurrentValue(sensorname).toString();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return e.getMessage();
		}
	}

	/*
	 * this is the method for adding cureent values of diffrent sensors to mysql
	 * db
	 */
	@GET
	@Path("{userid}/{sensorname}/getalarmcount")
	@Produces("text/plain")
	public String GetAlarmCount(@PathParam("userid") String userid,
			@PathParam("sensorname") String sensorname) throws SQLException {
		try {

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetAlarmCount\n");
			return rm.getAlarmCount(sensorname, userid).toString();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return e.getMessage();
		}
	}

	@GET
	@Path("{userid}/{sensortypename}/addsensortype")
	public Response addsensortype(@PathParam("userid") String userid,
			@PathParam("sensortypename") String sensortypename) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addsensortype\n");
			sensortypename = URLDecoder.decode(sensortypename, "UTF-8");
			int sta = dbo.addsensortype(sensortypename);
			if (sta == 1) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": Sensor type inserted successfully\n");
				return Response.status(200).entity("ok").build();
			} else {
				return Response.status(200).entity("0").build();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}

	}

	@GET
	@Path("{userid}/{sensortype}/deletesensortype")
	@Produces("text/plain")
	public Response deletesensortype(@PathParam("userid") String userid,
			@PathParam("sensortype") String sensortype) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deletesensortype\n");
			sensortype = URLDecoder.decode(sensortype, "UTF-8");
			int sensorcount = dbo.getsensorcount(sensortype);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": get sensor count with this type\n");
			int profilecount = dbo.getprofilecount(sensortype);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": get graph profile count with this type\n");
			if (sensorcount > 0) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": sensor found with this type\n");
				return Response.status(200).entity("s1").build();
			} else if (profilecount > 0) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": graph profile found with this type\n");
				return Response.status(200).entity("p1").build();
			} else {
				dbo.deletesensortype(sensortype);
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": Sensor type deleted successfully..\n");
				return Response.status(200).entity("ok").build();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements3(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee3(Document doc, String sensortype) {
		Element employee = doc.createElement("sensor");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements3(doc, employee, "sensortype",
				sensortype));

		return employee;
	}

	/**
	 * Function getUserList is used to retrieve user details.
	 * 
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("/getSensorType")
	@Produces("application/xml")
	public Response getSensorType() {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorType\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/sensor", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<String> sensortypelist = new ArrayList<String>();
			sensortypelist = dbo.getsensortype();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor types from db is completed\n");
			for (int i = 0; i < sensortypelist.size(); i++) {

				rootElement
						.appendChild(getEmployee3(doc, sensortypelist.get(i)));
			}

			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	@GET
	@Path("{userid}/{graphprofile}/{sensortype}/{xlabel}/{ylabel}/{min}/{max}/addgraphprofile")
	public Response addgraphprofile(@PathParam("userid") String userid,
			@PathParam("graphprofile") String graphprofile,
			@PathParam("sensortype") String sensortype,
			@PathParam("xlabel") String xlabel,
			@PathParam("ylabel") String ylabel, @PathParam("min") String min,
			@PathParam("max") String max) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addgraphprofile\n");

			graphprofile = URLDecoder.decode(graphprofile, "UTF-8");
			sensortype = URLDecoder.decode(sensortype, "UTF-8");
			xlabel = URLDecoder.decode(xlabel, "UTF-8");
			ylabel = URLDecoder.decode(ylabel, "UTF-8");

			int st = dbo.addgraphprofile(graphprofile, sensortype, xlabel,
					ylabel, Double.parseDouble(min), Double.parseDouble(max));
			if (st == 1) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": graph profile inserted successfully..\n");
				return Response.status(200).entity("ok").build();
			} else {
				return Response.status(200).entity("0").build();
			}

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}

	}

	@GET
	@Path("{userid}/{profilename}/deletegraphprofile")
	@Produces("text/plain")
	public Response deletegraphprofile(@PathParam("userid") String userid,
			@PathParam("profilename") String profilename) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deletegraphprofile\n");
			profilename = URLDecoder.decode(profilename, "UTF-8");
			int sensorgraphcount = dbo.sensorgraphcount(profilename);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": get sensor graph count with this profile\n");
			if (sensorgraphcount > 0) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": sensor found with this graph profile\n");
				return Response.status(200).entity("s1").build();
			} else {
				dbo.deleteGraphprofile(profilename);
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": graph profile deleted successfully..\n");
				return Response.status(200).entity("ok").build();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	@GET
	@Path("{profile}/{xlabel}/{ylabel}/{min}/{max}/updategraphprofile")
	@Produces("text/plain")
	public Response updategraphprofile(@PathParam("profile") String profile,
			@PathParam("xlabel") String xlabel,
			@PathParam("ylabel") String ylabel, @PathParam("min") String min,
			@PathParam("max") String max) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updategraphprofile\n");

			profile = URLDecoder.decode(profile, "UTF-8");
			xlabel = URLDecoder.decode(xlabel, "UTF-8");
			ylabel = URLDecoder.decode(ylabel, "UTF-8");

			dbo.editGraphProfile(profile, xlabel, ylabel, min, max);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile updated successfully..\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements4(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee4(Document doc, String graphprofile,
			String sensortype, String xlabel, String ylabel, Double min,
			Double max) {
		Element employee = doc.createElement("graph");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements4(doc, employee, "graphprofile",
				graphprofile));
		employee.appendChild(getUserElements4(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements4(doc, employee, "xlabel", xlabel));
		employee.appendChild(getUserElements4(doc, employee, "ylabel", ylabel));
		employee.appendChild(getUserElements4(doc, employee, "min",
				min.toString()));
		employee.appendChild(getUserElements4(doc, employee, "max",
				max.toString()));

		return employee;
	}

	@GET
	@Path("/getProfileList")
	@Produces("application/xml")
	public Response getProfileList() {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getProfileList\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<GraphProfile> mylist = dbo.getGraphProfile();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading profile list from db is completed\n");
			for (GraphProfile m : mylist) {
				rootElement.appendChild(getEmployee4(doc, m.getGraphprofile(),
						m.getSensortype(), m.getXlabel(), m.getYlabel(),
						m.getMin(), m.getMax()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements5(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee5(Document doc, String graphprofile,
			String sensortype, String xlabel, String ylabel, Double min,
			Double max) {
		Element employee = doc.createElement("graph");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements5(doc, employee, "graphprofile",
				graphprofile));
		employee.appendChild(getUserElements5(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements5(doc, employee, "xlabel", xlabel));
		employee.appendChild(getUserElements5(doc, employee, "ylabel", ylabel));
		employee.appendChild(getUserElements5(doc, employee, "min",
				min.toString()));
		employee.appendChild(getUserElements5(doc, employee, "max",
				max.toString()));

		return employee;
	}

	@GET
	@Path("{sensortype}/GetProfileByType")
	@Produces("application/xml")
	public Response GetProfileByType(@PathParam("sensortype") String sensortype) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetProfileByType\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<GraphProfile> mylist = dbo
					.getGraphProfilebytype(sensortype);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading profile list by type from db is completed\n");
			for (GraphProfile m : mylist) {
				rootElement.appendChild(getEmployee5(doc, m.getGraphprofile(),
						m.getSensortype(), m.getXlabel(), m.getYlabel(),
						m.getMin(), m.getMax()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements6(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee6(Document doc, String graphprofile,
			String sensortype, String xlabel, String ylabel, Double min,
			Double max) {
		Element employee = doc.createElement("graph");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements6(doc, employee, "graphprofile",
				graphprofile));
		employee.appendChild(getUserElements6(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements6(doc, employee, "xlabel", xlabel));
		employee.appendChild(getUserElements6(doc, employee, "ylabel", ylabel));
		employee.appendChild(getUserElements6(doc, employee, "min",
				min.toString()));
		employee.appendChild(getUserElements6(doc, employee, "max",
				max.toString()));

		return employee;
	}

	@GET
	@Path("{profilename}/GetProfileByName")
	@Produces("application/xml")
	public Response GetProfileByName(
			@PathParam("profilename") String profilename) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetProfileByName\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<GraphProfile> mylist = dbo
					.getGraphProfilebyname(profilename);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading profile list by sensor name from db is completed\n");
			for (GraphProfile m : mylist) {
				rootElement.appendChild(getEmployee6(doc, m.getGraphprofile(),
						m.getSensortype(), m.getXlabel(), m.getYlabel(),
						m.getMin(), m.getMax()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements7(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee7(Document doc, String sensorname,
			String profilename) {
		Element employee = doc.createElement("sensorgraph");
		// employee.setAttribute("userid", userid);
		employee.appendChild(getUserElements7(doc, employee, "sensorname",
				sensorname));
		employee.appendChild(getUserElements7(doc, employee, "profilename",
				profilename));

		return employee;
	}

	@GET
	@Path("{sensorname}/GetSensorGraphBysensor")
	@Produces("application/xml")
	public Response GetSensorGraphBysensor(
			@PathParam("sensorname") String sensorname) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetSensorGraphBysensor\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<SensorGraph> mylist = dbo.getsensorgraph(sensorname);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor graph by sensor name from db is completed\n");
			for (SensorGraph m : mylist) {
				rootElement.appendChild(getEmployee7(doc, m.getSensorname(),
						m.getProfilename()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements8(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee8(Document doc, String sensorname,
			String sensortype, String sensorinfo, String sensorlocation,
			String max, String min, String graphprofile, String xlabel,
			String ylabel, Double gmin, Double gmax, String latitude,
			String longitude) {
		Element employee = doc.createElement("sensor");
		employee.appendChild(getUserElements8(doc, employee, "sensorname",
				sensorname));
		employee.appendChild(getUserElements8(doc, employee, "sensortype",
				sensortype));
		employee.appendChild(getUserElements8(doc, employee, "sensorinfo",
				sensorinfo));
		employee.appendChild(getUserElements8(doc, employee, "sensorlocation",
				sensorlocation));
		employee.appendChild(getUserElements8(doc, employee, "max", max));
		employee.appendChild(getUserElements8(doc, employee, "min", min));
		employee.appendChild(getUserElements8(doc, employee, "graphprofile",
				graphprofile));
		employee.appendChild(getUserElements8(doc, employee, "xlabel", xlabel));
		employee.appendChild(getUserElements8(doc, employee, "ylabel", ylabel));
		employee.appendChild(getUserElements8(doc, employee, "gmin",
				gmin.toString()));
		employee.appendChild(getUserElements8(doc, employee, "gmax",
				gmax.toString()));
		employee.appendChild(getUserElements8(doc, employee, "latitude",
				latitude));
		employee.appendChild(getUserElements8(doc, employee, "longitude",
				longitude));
		return employee;
	}

	/**
	 * Function getUserList is used to retrieve user details.
	 * 
	 * @return Response : Return ok string.
	 * @throws Exception
	 *             : Return nok string.
	 */
	@GET
	@Path("{userid}/{sensorname}/getSensorListByName")
	@Produces("application/xml")
	public Response getSensorListByName(@PathParam("userid") String userid,
			@PathParam("sensorname") String sensorname

	) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorListByName\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/sensor", "OpenIOT");
			doc.appendChild(rootElement);

			ArrayList<SensorData> mylist = new ArrayList<SensorData>();
			for (Map.Entry<String, sensormetadata> entry : sensormap.entrySet()) {
				String id = entry.getValue().getUserid();
				if (userid.equals(id)) {
					String name = entry.getValue().getSensorid();
					if (name.equals(sensorname))
						mylist.add(new SensorData(entry.getValue()
								.getSensorid(), entry.getValue()
								.getSensortype(), entry.getValue()
								.getSensorinfo(), entry.getValue()
								.getLocation(), String.valueOf(entry.getValue()
								.getSensormax()), String.valueOf(entry
								.getValue().getSensormin()), entry.getValue()
								.getSensorlati(), entry.getValue()
								.getSensorlongi()));
				}
			}

			ArrayList<SensorGraph> mylist1 = dbo.getsensorgraph(sensorname);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading sensor graph by sensor name from db is completed\n");
			String profilename = mylist1.get(0).getProfilename();
			ArrayList<GraphProfile> mylist2 = dbo
					.getGraphProfilebyname(profilename);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading graph profile by name from db is completed\n");
			rootElement
					.appendChild(getEmployee8(doc, mylist.get(0).getSensorid(),
							mylist.get(0).getSensortype(), mylist.get(0)
									.getSensorinfo(), mylist.get(0)
									.getSensorlocation(), mylist.get(0)
									.getMax(), mylist.get(0).getMin(), mylist2
									.get(0).getGraphprofile(), mylist2.get(0)
									.getXlabel(), mylist2.get(0).getYlabel(),
							mylist2.get(0).getMin(), mylist2.get(0).getMax(),
							mylist.get(0).getLatitude(), mylist.get(0)
									.getLongitude()));

			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	@GET
	@Path("{sensorname}/{alarmstatus}/addalarmstatus")
	public Response addalarmstatus(@PathParam("sensorname") String sensorname,
			@PathParam("alarmstatus") String alarmstatus) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addalarmstatus\n");
			sensormetadata md = new sensormetadata();
			md = sensormap.get(sensorname);
			md.setSensorstatus(alarmstatus);
			dbo.updateAlarmStatus(sensorname, alarmstatus);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm status updated successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("{userid}/{enddeviceid}/{enddeviceinfo}/{latitude}/{longitude}/{location}/{timeinterval}/registerEndPoint")
	@Produces("text/plain")
	public Response registerEndPoint(@PathParam("userid") String userid,
			@PathParam("enddeviceid") String enddeviceid,
			@PathParam("enddeviceinfo") String enddeviceinfo,
			@PathParam("latitude") String latitude,
			@PathParam("longitude") String longitude,
			@PathParam("location") String location,
			@PathParam("timeinterval") String timeinterval) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> registerEndPoint\n");
			enddeviceid = URLDecoder.decode(enddeviceid, "UTF-8").toLowerCase();
			location = URLDecoder.decode(location, "UTF-8");
			enddeviceinfo = URLDecoder.decode(enddeviceinfo, "UTF-8");
			int sta = dbo.addEndDevice(enddeviceid, enddeviceinfo, latitude,
					longitude, location, timeinterval);
			if (sta == 1) {
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": registerEndPoint thread started successfully \n");
				return Response.status(200).entity("ok").build();
			} else {
				return Response.status(200).entity("0").build();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements9(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee9(Document doc, String EndPointDeviceId,
			String DeviceInfo, String latitude, String longitude,
			String location, String timeintervel) {
		Element employee = doc.createElement("EndPoint");
		employee.appendChild(getUserElements9(doc, employee,
				"EndPointDeviceId", EndPointDeviceId));
		employee.appendChild(getUserElements9(doc, employee, "DeviceInfo",
				DeviceInfo));
		employee.appendChild(getUserElements9(doc, employee, "latitude",
				latitude));
		employee.appendChild(getUserElements9(doc, employee, "longitude",
				longitude));
		employee.appendChild(getUserElements9(doc, employee, "location",
				location));
		employee.appendChild(getUserElements9(doc, employee, "timeintervel",
				timeintervel));

		return employee;
	}

	@GET
	@Path("/GetEndPointDevice")
	@Produces("application/xml")
	public Response GetEndPointDevice() {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetEndPointDevice\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<EndPointDevice> mylist = dbo.getEndPointDevice();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading endpoint list from db is completed\n");
			for (EndPointDevice m : mylist) {
				rootElement
						.appendChild(getEmployee9(doc, m.getEnddeviceid(),
								m.getEnddeviceinfo(), m.getLatitude(),
								m.getLongitude(), m.getLocation(),
								m.getTimeintervel()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	@GET
	@Path("{endpointdevicename}/deleteEndPoint")
	@Produces("text/plain")
	public Response deleteEndPoint(
			@PathParam("endpointdevicename") String endpointdevicename) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteEndPoint\n");
			dbo.deleteEndPoint(endpointdevicename);

			logger.info(loggerdateFormat.format(loggerdate)
					+ ":EndPoint Deleted successfully\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements10(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee10(Document doc, String EndPointDeviceId,
			String DeviceInfo, String latitude, String longitude,
			String location, String timeintervel) {
		Element employee = doc.createElement("EndPoint");
		employee.appendChild(getUserElements10(doc, employee,
				"EndPointDeviceId", EndPointDeviceId));
		employee.appendChild(getUserElements10(doc, employee, "DeviceInfo",
				DeviceInfo));
		employee.appendChild(getUserElements10(doc, employee, "latitude",
				latitude));
		employee.appendChild(getUserElements10(doc, employee, "longitude",
				longitude));
		employee.appendChild(getUserElements10(doc, employee, "location",
				location));
		employee.appendChild(getUserElements9(doc, employee, "timeintervel",
				timeintervel));
		return employee;
	}

	@GET
	@Path("{endpointdevicename}/GetEndPointDeviceByName")
	@Produces("application/xml")
	public Response GetEndPointDeviceByName(
			@PathParam("endpointdevicename") String endpointdevicename) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetEndPointDeviceByName\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<EndPointDevice> mylist = dbo
					.getEndPointDeviceByName(endpointdevicename);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading profile list by sensor name from db is completed\n");
			for (EndPointDevice m : mylist) {
				rootElement
						.appendChild(getEmployee10(doc, m.getEnddeviceid(),
								m.getEnddeviceinfo(), m.getLatitude(),
								m.getLongitude(), m.getLocation(),
								m.getTimeintervel()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	@GET
	@Path("{EndPointDeviceId}/{DeviceInfo}/{latitude}/{longitude}/{location}/{timeintervel}/updateEndPoint")
	@Produces("text/plain")
	public Response updateEndPoint(
			@PathParam("EndPointDeviceId") String EndPointDeviceId,
			@PathParam("DeviceInfo") String DeviceInfo,
			@PathParam("latitude") String latitude,
			@PathParam("longitude") String longitude,
			@PathParam("location") String location,
			@PathParam("timeintervel") String timeintervel) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateEndPoint\n");

			DeviceInfo = URLDecoder.decode(DeviceInfo, "UTF-8");
			location = URLDecoder.decode(location, "UTF-8");

			dbo.editEndDevice(EndPointDeviceId, DeviceInfo, latitude,
					longitude, location, timeintervel);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile updated successfully..\n");
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

	private static Node getUserElements11(Document doc, Element element,
			String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getEmployee11(Document doc, String sensorname) {
		Element employee = doc.createElement("EndPoint");
		employee.appendChild(getUserElements11(doc, employee, "sensorname",
				sensorname));

		return employee;
	}

	@GET
	@Path("{endpointdevicename}/GetsensorNameByEndPointDevice")
	@Produces("application/xml")
	public Response GetsensorNameByEndPointDevice(
			@PathParam("endpointdevicename") String endpointdevicename) {

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> GetEndPointDeviceByName\n");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElementNS(
					"http://www.gadgeon.com/user", "OpenIOT");
			doc.appendChild(rootElement);
			ArrayList<SensorListByEndDevice> mylist = dbo
					.getSensorNameByEndPointDevice(endpointdevicename);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": reading profile list by sensor name from db is completed\n");
			for (SensorListByEndDevice m : mylist) {
				rootElement.appendChild(getEmployee11(doc, m.getSensorname()));
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": xml created successfully\n");
			return Response.status(200).entity(source).build();

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}
	
	
	@GET
	@Path("{userid}/{sensorid}/getCurrentValue")
	@Produces("text/plain")
	public Response getCurrentValue(@PathParam("userid") String userid,
			@PathParam("sensorid") String sensorid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getCurrentValue\n");
			sensorid = URLDecoder.decode(sensorid, "UTF-8").toLowerCase();
			 HashMap<String, Date> StatusMap = StatusSubscribe.getStatus();
			 
			 
			 Date value = StatusMap.get(sensorid);
				if (value == null) {
					return Response.status(200).entity("offline").build();
				} 
				
				
			PublishMqtt publishMqtt=new PublishMqtt();
			sensorid=sensorid.replace("_", "/");
			publishMqtt.publishMqtt(sensorid,"data");
			
			return Response.status(200).entity("ok").build();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return Response.status(200).entity("nok").build();
		}
	}

}