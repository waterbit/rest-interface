package com.gadgeon.dataRetrieval;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gadgeon.dbManager.dbOperation;
import com.gadgeon.email.EmailService;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.sensormetadata;

public class retrievalManager {

	static Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
	static Map<String, sensormetadata> sensormap1 = new HashMap<String, sensormetadata>();
	static List<String> sensorlist = new ArrayList<String>();
	EmailService es = new EmailService();
	dbOperation dbo = new dbOperation();
	final static Logger logger = Logger.getLogger(retrievalManager.class);
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();

	/**
	 * Function is used to read alarm count from database
	 * 
	 * @param sensorid
	 *            : A string containing sensor id to check
	 * @return alarmcnt : A double value containing alarm count
	 * @throws SQLException
	 */

	public static Map<String, sensormetadata> returnMap() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> returnMap\n");
			loadDataMap map = new loadDataMap();
			sensormap1 = map.getSensormap();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor map returned successfully\n");

			return sensormap1;
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return null;
		}

	}

	public Double getAlarmCount(String sensorid, String userid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getAlarmCount\n");

			sensormetadata ds = new sensormetadata();
			loadDataMap map = new loadDataMap();
			sensormap = map.getSensormap();
			ds = sensormap.get(sensorid);
			String user = ds.getUserid();
			Double alarmcnt = null;
			if (userid.equals(user))
				alarmcnt = ds.getSensoralarmcnt();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm count returned successfully\n");

			return alarmcnt;

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return null;
		}
	}

	/**
	 * Function is used to read current value from database
	 * 
	 * @param sensorid
	 *            : A string containing sensor id to check
	 * @return crntvalue : A double value containing current value
	 * @throws SQLException
	 */

	public Double getCurrentValue(String sensorid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getCurrentValue\n");

			sensormetadata ds = new sensormetadata();
			loadDataMap map = new loadDataMap();
			sensormap = map.getSensormap();
			ds = sensormap.get(sensorid);

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": current value returned successfully\n");

			return ds.getSensorcrntval();
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return null;
		}
	}

	/**
	 * Function is used to delete the sensor from map
	 * 
	 * @param userid
	 *            : A string containing user id to check
	 * @throws SQLException
	 */

	public void deleteSensor(int userid) {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteSensor\n");
			sensormetadata ds = new sensormetadata();
			loadDataMap map = new loadDataMap();
			sensormap = map.getSensormap();
			ds = sensormap.get(userid);
			sensormap.remove(ds);

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor deleted successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");

		}
	}

}