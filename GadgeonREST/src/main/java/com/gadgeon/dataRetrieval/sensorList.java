package com.gadgeon.dataRetrieval;

public class sensorList {

	private String sensorid;
	private String sensortype;
	private String sensorinfo;
	private double sensoralarmcnt;
	
	public sensorList(String sensorid, String sensortype, String sensorinfo, double sensoralarmcnt) {
	    this.setSensorid(sensorid);
	    this.setSensortype(sensortype);
	    this.setSensorinfo(sensorinfo);
	    this.setSensoralarmcnt(sensoralarmcnt);
	  }

	public String getSensorid() {
		return sensorid;
	}

	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}

	public String getSensortype() {
		return sensortype;
	}

	public void setSensortype(String sensortype) {
		this.sensortype = sensortype;
	}

	public String getSensorinfo() {
		return sensorinfo;
	}

	public void setSensorinfo(String sensorinfo) {
		this.sensorinfo = sensorinfo;
	}

	public double getSensoralarmcnt() {
		return sensoralarmcnt;
	}

	public void setSensoralarmcnt(double sensoralarmcnt) {
		this.sensoralarmcnt = sensoralarmcnt;
	}


}
