package com.gadgeon.sensor;

import java.io.IOException;

import com.gadgeon.dbManager.dbOperation;

public class SensorDelete implements Runnable {
	
	public dbOperation dbo = new dbOperation();
	private String sensorname;
	
	public  SensorDelete(String sensorname)
	{
		this.sensorname=sensorname;
	}
	
	public void run()
	{
		
		try {
			deleteFromDb();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			deleteFromGSN();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void deleteFromDb() throws Exception {
		// TODO Auto-generated method stub
		dbo.deleteSensor(sensorname);
	}

	private void deleteFromGSN() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		String curl1 = "curl -v http://localhost:22001/vs/vsensor/"+sensorname+"/delete -X GET";
		
		Process p1= Runtime.getRuntime().exec(curl1);
		p1.waitFor();

	}

}
