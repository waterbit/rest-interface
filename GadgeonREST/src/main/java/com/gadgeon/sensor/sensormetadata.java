package com.gadgeon.sensor;

public class sensormetadata {
	
	@SaveToDatabase
	protected String sensorid="";
	@SaveToDatabase
	protected String sensortype="";
	@SaveToDatabase
	protected String sensorinfo="";
	@SaveToDatabase
	protected String sensorunit="";
	@SaveToDatabase
	protected double sensormin;
	@SaveToDatabase
	protected double sensormax;
	@SaveToDatabase
	protected String sensorlati="";
	@SaveToDatabase
	protected String sensorlongi="";
	@SaveToDatabase
	protected double sensoralarmcnt=0;
	@SaveToDatabase
	protected double sensorcrntval=0;
	@SaveToDatabase
	protected String sensorstatus="";
	@SaveToDatabase
	protected int notify=0;
	@SaveToDatabase
	protected String userid="";
	@SaveToDatabase
	protected String sensorname="";
	@SaveToDatabase
	private String location="";
	@SaveToDatabase
	private String field="";
	@SaveToDatabase
	private String lastuptime="";
	@SaveToDatabase
	private String enddevice="";
	
	
	
	
	public String getEnddevice() {
		return enddevice;
	}
	public void setEnddevice(String enddevice) {
		this.enddevice = enddevice;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	public String getLastuptime() {
		return lastuptime;
	}
	public void setLastuptime(String lastuptime) {
		this.lastuptime = lastuptime;
	}
	public String getSensorname() {
		return sensorname;
	}
	public void setSensorname(String sensorname) {
		this.sensorname = sensorname;
	}
	public String getSensorid() {
		return sensorid;
	}
	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}
	public String getSensortype() {
		return sensortype;
	}
	public void setSensortype(String sensortype) {
		this.sensortype = sensortype;
	}
	public String getSensorinfo() {
		return sensorinfo;
	}
	public void setSensorinfo(String sensorinfo) {
		this.sensorinfo = sensorinfo;
	}
	public String getSensorunit() {
		return sensorunit;
	}
	public void setSensorunit(String sensorunit) {
		this.sensorunit = sensorunit;
	}
	public double getSensormin() {
		return sensormin;
	}
	public void setSensormin(double sensormin) {
		this.sensormin = sensormin;
	}
	public double getSensormax() {
		return sensormax;
	}
	public void setSensormax(double sensormax) {
		this.sensormax = sensormax;
	}
	public String getSensorlati() {
		return sensorlati;
	}
	public void setSensorlati(String sensorlati) {
		this.sensorlati = sensorlati;
	}
	public String getSensorlongi() {
		return sensorlongi;
	}
	public void setSensorlongi(String sensorlongi) {
		this.sensorlongi = sensorlongi;
	}
	public double getSensoralarmcnt() {
		return sensoralarmcnt;
	}
	public void setSensoralarmcnt(double sensoralarmcnt) {
		this.sensoralarmcnt = sensoralarmcnt;
	}
	public double getSensorcrntval() {
		return sensorcrntval;
	}
	public void setSensorcrntval(double sensorcrntval) {
		this.sensorcrntval = sensorcrntval;
	}
	public String getSensorstatus() {
		return sensorstatus;
	}
	public void setSensorstatus(String sensorstatus) {
		this.sensorstatus = sensorstatus;
	}
	public int getNotify() {
		return notify;
	}
	public void setNotify(int notify) {
		this.notify = notify;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	

}
