package com.gadgeon.timer;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.gadgeon.dbManager.dbOperation;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.sensormetadata;

public class LastUpTime extends Thread {

	public void startcheck() {
		MyTimerTask timerTask = new MyTimerTask();
		new Timer().scheduleAtFixedRate(timerTask, 0, 60000);

	}
}

class MyTimerTask extends TimerTask {
	final static Logger logger = Logger.getLogger(MyTimerTask.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();

	public void run() {

		updateLastTime();
	}

	private void updateLastTime() {
		// TODO Auto-generated method stub
		try {
			loadDataMap mp = new loadDataMap();
			Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
			sensormap = mp.getSensormap();

			Iterator<Entry<String, sensormetadata>> itr = sensormap.entrySet()
					.iterator();
			while (itr.hasNext()) {

				Entry<String, sensormetadata> pairs = itr.next();
				String sensorname = pairs.getKey();
				sensormetadata metadata = sensormap.get(sensorname);
				dbOperation dbop = new dbOperation();

				if (metadata.getLastuptime() != "") {
					dbop.updateLastTime(metadata);
					logger.info(loggerdateFormat.format(loggerdate)
							+ ": manaafffffffff \n" + metadata.getLastuptime());
				}

			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": last time updated \n");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage());
		}

	}

}