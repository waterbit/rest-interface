package com.gadgeon.mosquitto;

import java.io.*;	
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.sensormetadata;



public class Subscribe implements MqttCallback 
{
	public static HashMap<String, Date> StatusMap = new HashMap<String, Date>();
	static boolean stop = false;
	public loadDataMap mp = new loadDataMap();
	public Subscribe()
	{
		
	}
	
	public  void startsubcribe() throws IOException
	{
		String topic = "status/#";
		String broker = "54.69.11.117";
		String port = "1883";
		String clientId ="Client-1_"+System.nanoTime();
		int qos = 1;

		
		String url = "tcp://"+broker+":"+port;
		
		
		try 
		{
			// Create an instance of the Subscribe client wrapper
			Subscribe sampleClient = new Subscribe(url,clientId);
			sampleClient.subscribe(topic,qos);
		}
		catch(MqttException me) 
		{
			me.printStackTrace();
		}
	}
    
	// Private instance variables
	private MqttClient client;
	private String brokerUrl;
	private String Topic;
	private int QOS;
	
	
	
	
	/**
	 * Constructs an instance of the sample client wrapper
	 * @param brokerUrl the url to connect to
	 * @param clientId the client id to connect with
	 * @throws MqttException
	 */
    public Subscribe(String brokerUrl, String clientId) throws MqttException 
    {
    	this.brokerUrl = brokerUrl;
    	
    	try 
    	{
    		// Construct the MqttClient instance
			client = new MqttClient(this.brokerUrl,clientId);
			// Set this wrapper as the callback handler
	    	client.setCallback(this);
		} 
    	catch (MqttException e) 
    	{
			//e.printStackTrace();
			log("Unable to set up client: "+e.toString());
			System.exit(1);
		}
    }

        
    /**
     * Subscribes to a topic and blocks until Enter is pressed
     * @param topicName the topic to subscribe to
     * @param qos the qos to subscibe at
     * @throws MqttException
     * @throws UnsupportedEncodingException 
     * @throws FileNotFoundException 
     */
    public void subscribe(String topicName, int qos) throws MqttException, FileNotFoundException, UnsupportedEncodingException 
    {
    	this.Topic = topicName;
    	this.QOS = qos;
    	log("Connecting to "+brokerUrl);
    	try 
    	{
	    	// Connect to the server
	    	client.connect();
    	}
    	catch (MqttException e) 
    	{
			//e.printStackTrace();
			log("Unable to connect: "+e.toString());
			System.exit(1);
		}

    	log("Connected !!!");
    	// Subscribe to the topic
    	log("Subscribing to topic \""+topicName+"\" qos "+qos);
    	client.subscribe(topicName, qos);

    	// Block until Enter is pressed
    	while(!stop){
			 System.out.println("------------ List of Active Sensor's --------------");
			 Date currentTime = new Date();
			
			 Iterator<Entry<String, Date>> itr = StatusMap.entrySet().iterator();
			 while (itr.hasNext()) {
			        Entry<String, Date> pairs = itr.next();
			        long diff = currentTime.getTime()- pairs.getValue().getTime();
			        if(diff>6000L)
			        	itr.remove();
			        System.out.println(pairs.getKey());
			    
			        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");	
			    	Date date = new Date();
			    	Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
			    	
			    	sensormap=  mp.getSensormap();
			    	sensormetadata metadata= sensormap.get(pairs.getKey());
			    	metadata.setLastuptime(dateFormat.format(date));
			        
			 }
			 
			
			 System.out.println("---------------------------------------------------");
			 try {
				 Thread.sleep(1000L);    // one second
			 }catch (Exception sleep) {}
		 }
		// Disconnect the client
		client.disconnect();
		log("Disconnected");
    }

    /**
     * Utility method to handle logging. If 'quietMode' is set, this method does nothing
     * @param message the message to log
     */
    private void log(String message) 
    {
    	System.out.println(message);
    }
	
	/****************************************************************/
	/* Methods to implement the MqttCallback interface              */
	/****************************************************************/
    
    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
    // Called when the connection to the server has been lost.
	public void connectionLost(Throwable cause) 
	{
		boolean connected = true;
		log("Connection to " + brokerUrl + " lost!");
		do
		{
			connected = true;
			log("Re - Connecting to "+brokerUrl);
	    	try 
	    	{
		    	// Connect to the server
		    	client.connect();
	    	}
	    	catch (MqttException e) 
	    	{
	    		connected = false;
				//e.printStackTrace();
				log("Unable to connect: "+e.toString());
				try {
					Thread.sleep(5000L);    // one second
				}catch (Exception sleep) {}  

			}
		}while(!connected);
		log("Connected to " + brokerUrl);
		log("Connected !!!");
    	// Subscribe to the topic
    	log("Subscribing to topic \""+Topic+"\" qos "+QOS);
    	try {
			client.subscribe(Topic, QOS);
		} catch (MqttSecurityException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log("Unable to subscribe: "+e.toString());
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log("Unable to subscribe: "+e.toString());
		}
    	log("Subscribed to topic \""+Topic+"\" qos "+QOS);
	}

	/**
	 * @see MqttCallback#deliveryComplete(MqttDeliveryToken)
	 */
	// Called when a message has completed delivery to the
	// server. The token passed in here is the same one
	// that was returned in the original call to publish.
	// This allows applications to perform asynchronous 
	// delivery without blocking until delivery completes.
	
	// This sample demonstrates synchronous delivery, by
	// using the token.waitForCompletion() call in the main thread.

	public void deliveryComplete(IMqttDeliveryToken token)
	{
		
	}

	
	/**
	 * @see MqttCallback#messageArrived(MqttTopic, MqttMessage)
	 */
	// Called when a message arrives from the server.
	//public void messageArrived(MqttTopic topic, MqttMessage message) throws MqttException 
	public void messageArrived(String topic, MqttMessage message) throws Exception
	{
		Date date = new Date();
		//System.out.println("------------ Status Message Received --------------");
		//System.out.println("Topic   - " + topic);
		//System.out.println("Time    - "+ date.toString());
		//System.out.println("QoS:\t\t" + message.getQos());
		//System.out.println("Message - " + new String(message.getPayload()));
		//System.out.println("Old Message ? " + message.isRetained());
		String macID, sensorName;
		int start = topic.indexOf('/') + 1;
		int end = topic.indexOf('/',start);
		macID = topic.substring(start, end);
		sensorName = topic.substring((end + 1), (topic.length() - 1));
		if(!(message.isRetained()))
			StatusMap.put(sensorName+"_"+macID, date);
		else
			System.out.println("Retained Message!!!");
		//System.out.println("---------------------------------------------------");
	}

	/****************************************************************/
	/* End of MqttCallback methods                                  */
	/****************************************************************/

}