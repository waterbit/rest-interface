package com.gadgeon.mosquitto;


import com.ibm.micro.client.mqttv3.MqttCallback;
import com.ibm.micro.client.mqttv3.MqttClient;
import com.ibm.micro.client.mqttv3.MqttDeliveryToken;
import com.ibm.micro.client.mqttv3.MqttException;
import com.ibm.micro.client.mqttv3.MqttMessage;
import com.ibm.micro.client.mqttv3.MqttTopic;

public class Publish implements MqttCallback 
{
	
	
	
	public  String send(String sensorname,String macid,String msg)
	{
		String topic=macid+"/"+sensorname;
		String message = msg;
		int qos = 0;
		String broker = "54.69.11.117";
		int port = 1883;
		
		String url = "tcp://"+broker+":"+port;
		String clientId = "Client-1_"+System.nanoTime();
		
				try 
				{
					
					Publish sampleClient = new Publish(url,clientId);
					sampleClient.publish(topic,qos,message.getBytes());
				} 
				catch(Exception me) 
				{
					me.printStackTrace();
				}
			
				return "ok";
	}
	
    // Private instance variables
    private MqttClient client;
    private String brokerUrl;
	
    /**
     * Constructs an instance of the sample client wrapper
     * @param brokerUrl the url to connect to
     * @param clientId the client id to connect with
     * @throws MqttException
     */
    public Publish(String brokerUrl, String clientId) throws MqttException 
    {
    	this.brokerUrl = brokerUrl;
    	try 
    	{
    		// Construct the MqttClient instance
    		client = new MqttClient(this.brokerUrl,clientId);
    		// Set this wrapper as the callback handler
	    	client.setCallback(this);
    	} 
    	catch (Exception e) 
		{
			e.printStackTrace();
			log("Unable to set up client: "+e.toString());
			System.exit(1);
		}	
    }

    public Publish() {
		// TODO Auto-generated constructor stub
	}

	/**
     * Performs a single publish
     * @param topicName the topic to publish to
     * @param qos the qos to publish at
     * @param payload the payload of the message to publish 
     * @throws MqttException
     */
    public void publish(String topicName, int qos, byte[] payload) throws MqttException 
    {
    	
    	// Connect to the server
    	log("Connecting to "+brokerUrl);
    	try 
    	{
	    	// Connect to the server
	    	client.connect();
    	}
    	catch (Exception e) 
    	{
			
			log("Unable to connect: "+e.toString());
			return;
		}
    	log("Connected !!!");
    	
    	// Get an instance of the topic
    	MqttTopic topic = client.getTopic(topicName);

    	// Construct the message to publish
    	MqttMessage message = new MqttMessage(payload);
    	message.setQos(qos);

    	// Publish the message
    	log("Publishing to topic \""+topicName+"\" qos "+qos);
    	MqttDeliveryToken token = topic.publish(message);

    	// Wait until the message has been delivered to the server
    	token.waitForCompletion();
    	
    	// Disconnect the client
    	client.disconnect();
    	log("Disconnected");
    }
    
    
    /**
     * Utility method to handle logging.
     * @param message the message to log
     */
    private void log(String message) 
    {
    	System.out.println(message);
    }


	
	/****************************************************************/
	/* Methods to implement the MqttCallback interface              */
	/****************************************************************/
    
    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
	// Called when the connection to the server has been lost.
	public void connectionLost(Throwable cause) 
	{
		log("Connection to " + brokerUrl + " lost!");
		System.exit(1);
	}

    /**
     * @see MqttCallback#deliveryComplete(MqttDeliveryToken)
     */
	// Called when a message has completed delivery to the
	// server. The token passed in here is the same one
	// that was returned in the original call to publish.
	// This allows applications to perform asychronous 
	// delivery without blocking until delivery completes.
	
	// This sample demonstrates synchronous delivery, by
	// using the token.waitForCompletion() call in the main thread.

	public void deliveryComplete(MqttDeliveryToken token) 
	{
		log("Message Delivered Successfully!!!");
	}

    /**
     * @see MqttCallback#messageArrived(MqttTopic, MqttMessage)
     */
	// Called when a message arrives from the server.
	public void messageArrived(MqttTopic topic, MqttMessage message) throws MqttException 
	{
		System.out.println("Topic:\t\t" + topic.getName());
        System.out.println("Message:\t" + new String(message.getPayload()));
        System.out.println("QoS:\t\t" + message.getQos());
	}

	/****************************************************************/
	/* End of MqttCallback methods                                  */
	/****************************************************************/
}
