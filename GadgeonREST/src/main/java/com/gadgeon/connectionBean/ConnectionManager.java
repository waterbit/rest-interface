package com.gadgeon.connectionBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Class is used for connecting with mysql database
 * 
 * @function getDbconnection : A function to connect the database
 * @function dbExists : A function to check the existence of database
 */

public class ConnectionManager {

	String url = "jdbc:mysql://localhost/";
	String dbName = "GadgeonSensorSchema";
	String driver = "com.mysql.jdbc.Driver";
	String userName = "gadgeon";
	String password = "gadgeon123";
	Connection conn = null;
	Statement statement = null;
	final static Logger logger = Logger.getLogger(ConnectionManager.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();

	/**
	 * Function is used to make a connection with mysql database
	 * 
	 * @return conn : A Connection to the database
	 */

	public Connection getDbConnection() {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getDbConnection\n");
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			
			 logger.info(loggerdateFormat.format(loggerdate)
						+ ": Mysql connection established successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return null;
		}
		return conn;

	}

}