package com.gadgeon.email;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.gadgeon.dataRetrieval.retrievalManager;

public class EmailService implements Runnable{
	static final String FROM = "shamlykh@gmail.com";   // Replace with your "From" address. This address must be verified.
    static final String TO = "manafcj@gmail.com";  // Replace with a "To" address. If you have not yet requested
                                                       // production access, this address must be verified.
    
  //  static final String TO = "issac.john@gadgeon.com";  // Replace with a "To" address. If you have not yet requested
    
    static final String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
    static final String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";
    
    // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
    static final String SMTP_USERNAME = "AKIAIWB67RP4ZAUBMS5Q";  // Replace with your SMTP username.
    static final String SMTP_PASSWORD = "ArdM1TTAcjB4uAHKGfNz2vkZM8+tdgszseMW1JCEDNvY ";  // Replace with your SMTP password.
    
    // Amazon SES SMTP host name. This example uses the us-east-1 region.
   static final String HOST = "email-smtp.us-west-2.amazonaws.com";    
  // static final String HOST=" email-smtp.eu-west-1.amazonaws.com";
    
    // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
    // STARTTLS to encrypt the connection.
    static final int PORT = 25;
    
    public String _message="";
    
	final static Logger logger = Logger.getLogger(EmailService.class);
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();
	
    public EmailService(){
        
    }
    
    public void run()  {
      
            try {
				mailservice(_message);
			} catch (AddressException e) {
				logger.error(loggerdateFormat.format(loggerdate) + ": "
						+ e.getMessage() + " \n");
			} catch (MessagingException e) {
				logger.error(loggerdateFormat.format(loggerdate) + ": "
						+ e.getMessage() + " \n");
			}
    } 
    
    
    
	public void mailservice(String _message) throws AddressException, MessagingException
	{
		  // Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	
    	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
    	// The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
    	props.put("mail.smtp.auth", "true");
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.starttls.required", "true");

        // Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM));
        msg.setReplyTo(new Address[]
        		{
        	    new InternetAddress("manafcj@gmail.com")
        	});
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(_message,"text/plain");
            
        // Create a transport.        
        Transport transport = session.getTransport();
                    
        // Send the message.
        try
        {
            System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
            
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
        	
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();        	
        }
    }
	}
