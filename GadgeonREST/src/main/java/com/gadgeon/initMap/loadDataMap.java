package com.gadgeon.initMap;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gadgeon.dbManager.dbOperation;
import com.gadgeon.sensor.sensormetadata;

/**
 * Class is used to load data to map from database
 */

public class loadDataMap {

	Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
	static Map<String, sensormetadata> sensormap1 = new HashMap<String, sensormetadata>();
	static List<String> sensorlist = new ArrayList<String>();

	final static Logger logger = Logger.getLogger(loadDataMap.class);
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();

	/**
	 * Function is used to load data from database to map
	 * 
	 * @throws SQLException
	 */
	public void loadMap() throws SQLException {
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> loadMap\n");
			sensormap.clear();
			dbOperation dbop = new dbOperation();
			sensorlist = dbOperation.getSensorList();
			if (sensorlist != null) {
				for (int i = 0; i < sensorlist.size(); i++) {
					sensormetadata da = new sensormetadata();
					da = dbop.loadData(sensorlist.get(i), da);
					sensormap.put(da.getSensorid(), da);
					setSensormap(sensormap);
				}
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": map loaded successfully\n");
		} catch (Exception e) {

			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");

		}
	}

	public Map<String, sensormetadata> getSensormap() {
		return sensormap1;
	}

	public void setSensormap(Map<String, sensormetadata> sensormap) {
		loadDataMap.sensormap1 = sensormap;
	}
}