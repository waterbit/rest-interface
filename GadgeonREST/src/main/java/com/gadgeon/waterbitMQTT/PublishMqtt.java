package com.gadgeon.waterbitMQTT;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class PublishMqtt {
	final static Logger logger = Logger.getLogger(MqttPublishTimer.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();
	
	public void publishMqtt(String topic,String msg) {
		int qos = 2;
		String broker = "tcp://52.0.88.104:1883";
		String clientId = "Client_" + System.nanoTime();
		MemoryPersistence persistence = new MemoryPersistence();

		try {
			MqttClient sampleClient = new MqttClient(broker, clientId,
					persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			logger.info("Connecting to broker: " + broker);
			sampleClient.connect(connOpts);
			logger.info("Connected");
			logger.info("Topic: " + topic);
			logger.info("Publishing message: " + msg);
			MqttMessage message = new MqttMessage(msg.getBytes());
			message.setQos(qos);
			sampleClient.publish(topic, message);
			logger.info("Message published");
			sampleClient.disconnect();
			logger.info("Disconnected");
		} catch (MqttException me) {
			logger.info("reason " + me.getReasonCode());
			logger.info("msg " + me.getMessage());
			logger.info("loc " + me.getLocalizedMessage());
			logger.info("cause " + me.getCause());
			logger.info("excep " + me);

			throw new IllegalArgumentException();

		}

	}

}