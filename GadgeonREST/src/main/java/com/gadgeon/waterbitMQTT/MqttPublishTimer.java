package com.gadgeon.waterbitMQTT;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.gadgeon.dbManager.EndPointDevice;
import com.gadgeon.dbManager.SensorListByEndDevice;
import com.gadgeon.dbManager.dbOperation;

public class MqttPublishTimer extends Thread {
	final static Logger logger = Logger.getLogger(MqttPublishTimer.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();

	public Map<String, Date> devicemap = new HashMap<>();

	public void startcheck() {
		setupSensorMap();
		MqttTimerTask timerTask = new MqttTimerTask();
		timerTask.setTempdevicemap(devicemap);
		new Timer().scheduleAtFixedRate(timerTask, 0, 2000);
	}

	public void setupSensorMap() {
		// logger.info("setupSensorMap working");
		try
		{
		dbOperation dbo = new dbOperation();
		List<EndPointDevice> list = dbo.getEndPointDevice();
		for (EndPointDevice ed : list) {
			Date date = new Date();
			devicemap.put(ed.getEnddeviceid(), date);
		}
		} catch (Exception e) {

			MqttPublishTimer mqttPublish = new MqttPublishTimer();
			mqttPublish.startcheck();

		}
	}

}

class MqttTimerTask extends TimerTask {
	public PublishMqtt publish = new PublishMqtt();

	final static Logger logger = Logger.getLogger(MqttTimerTask.class);
	public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public Date loggerdate = new Date();
	public Map<String, Date> tempdevicemap = new HashMap<>();

	public void setTempdevicemap(Map<String, Date> tempdevicemap) {
		this.tempdevicemap = tempdevicemap;
	}

	public void run() {

		publishRequest();
	}

	public void publishRequest() {
		try {
			dbOperation dbos = new dbOperation();

			logger.info("working ");
			Set set = tempdevicemap.entrySet();
			Iterator i = set.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();

				Date date1 = (Date) me.getValue();
				Date date2 = new Date();

				long diff = date2.getTime() - date1.getTime();
				long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);

				String devicename = (String) me.getKey();

				ArrayList<EndPointDevice> endPoints = dbos
						.getEndPointDeviceByName(devicename);
				int timeintervel = 60;
				if (endPoints != null) {
					timeintervel = Integer.parseInt(endPoints.get(0)
							.getTimeintervel());
				}

				if (timeintervel <= diffSeconds) {
					logger.info("Publish..... " + devicename);
					tempdevicemap.put(devicename, date2);
					dbOperation dbo = new dbOperation();
					List<SensorListByEndDevice> list = dbo
							.getSensorNameByEndPointDevice(devicename);
					publishRequestToEndDevice(list, devicename);
				}

			}
		} catch (Exception e) {

			MqttPublishTimer mqttPublish = new MqttPublishTimer();
			mqttPublish.startcheck();

		}

	}

	private void publishRequestToEndDevice(List<SensorListByEndDevice> list,
			String enddevice) {

		for (SensorListByEndDevice ed : list) {
			try {
				publish.publishMqtt(enddevice + "/" + ed.getSensorname(),
						"data");
			} catch (Exception exception) {
				try {
					Thread.sleep(5000L);
				} catch (Exception sleep) {
				}
				MqttPublishTimer mqttPublish = new MqttPublishTimer();
				mqttPublish.startcheck();

			}
		}
	}

}
