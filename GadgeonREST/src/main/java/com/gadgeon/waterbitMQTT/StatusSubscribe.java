package com.gadgeon.waterbitMQTT;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class StatusSubscribe {
	public static HashMap<String, Date> StatusMap = new HashMap<String, Date>();
	String topic = "status/#";
	String broker = "tcp://52.0.88.104:1883";
	String clientId = "Client_1" + System.nanoTime();
	MemoryPersistence persistence = new MemoryPersistence();
	int qos = 2;

	 final static Logger logger = Logger.getLogger(StatusSubscribe.class);
	 public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
	 "yyyy MMM dd HH:mm:ss");
	 public Date loggerdate = new Date();


	public void startSubscribe() {
		try {
			MqttClient sampleClient = new MqttClient(broker, clientId,
					persistence);
			sampleClient.setCallback(new MqttCallback() {

				@Override
				public void messageArrived(String topic, MqttMessage msg)
						throws Exception {
					logger.info("topic:" + topic);
					logger.info("msg:" + new String(msg.getPayload()));

						statusCheck(topic, new String(msg.getPayload()));

				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
					logger.info("delivary complete");
				}

				@Override
				public void connectionLost(Throwable cause) {
					try
					{
			    		MqttClient sampleClient = new MqttClient(broker, clientId,
								persistence);
				    	// Connect to the server
			    	
					
					boolean connected = true;
					logger.info("Connection to " + broker + " lost!");
					do
					{
						connected = true;
						logger.info("Re - Connecting to "+broker);
				    	try 
				    	{
					    	// Connect to the server
				    		sampleClient.connect();
				    	}
				    	catch (MqttException e) 
				    	{
				    		connected = false;
				    		logger.info("Unable to connect: "+e.toString());
							try {
								Thread.sleep(5000L);    // one second
							}catch (Exception sleep) {}  

						}
					}while(!connected);
					logger.info("Connected to " + broker);
					logger.info("Connected !!!");
			    	// Subscribe to the topic
					logger.info("Subscribing to topic \""+topic+"\" qos "+qos);
			    	try {
			    		sampleClient.subscribe(topic, qos);
					} catch (MqttSecurityException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.info("Unable to subscribe: "+e.toString());
					} catch (MqttException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.info("Unable to subscribe: "+e.toString());
					}
			    	logger.info("Subscribed to topic \""+topic+"\" qos "+qos);
					}
			    	catch (MqttException e) 
			    	{
			    		logger.info("Unable to connect: "+e.toString());
						try {
							Thread.sleep(5000L);    // one second
						}catch (Exception sleep) {}  

					}
				
				}
					
			});

			MqttConnectOptions options = new MqttConnectOptions();
			options.setUserName("admin");
			options.setPassword(new char[] { 'p', 'a', 's', 's', 'w', 'o', 'r',
					'd' });
			sampleClient.connect(options);
			sampleClient.subscribe(topic, qos);
			logger.info("Subscribing Started");
		} catch (MqttException me) {
			logger.info("reason " + me.getReasonCode());
			logger.info("msg " + me.getMessage());
			logger.info("loc " + me.getLocalizedMessage());
			logger.info("cause " + me.getCause());
			logger.info("excep " + me);
			me.printStackTrace();
		}
	}
	
	
	private static void statusCheck(String sensorname,String value) {
		try {
			
			logger.info("topic:" + sensorname);
			logger.info("msg:" + value);
			sensorname = sensorname.replace("status/", "");
			sensorname = sensorname.replace("/", "_");
			logger.info("sensorName=" + sensorname);
			
			
			StatusMap.put(sensorname, new Date());

			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	
	public static HashMap<String, Date> getStatus() {
		try {
			 Date currentTime = new Date();	
			 Iterator<Entry<String, Date>> itr = StatusMap.entrySet().iterator();
			 while (itr.hasNext()) {
			        Entry<String, Date> pairs = itr.next();
			        long diff = currentTime.getTime()- pairs.getValue().getTime();
			        if(diff>20000L)
			        	itr.remove();
			        logger.info(pairs.getKey());      
			 }			
		} catch (Exception ex) {
//			ex.printStackTrace();
		}
		return StatusMap;
	}

}