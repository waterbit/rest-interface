package com.gadgeon.waterbitMQTT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MqttSubscribe {
	String topic = "data/#";
	String broker = "tcp://52.0.88.104:1883";
	String clientId = "Client_1" + System.nanoTime();
	MemoryPersistence persistence = new MemoryPersistence();
	int qos = 2;

	 final static Logger logger = Logger.getLogger(MqttSubscribe.class);
	 public SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
	 "yyyy MMM dd HH:mm:ss");
	 public Date loggerdate = new Date();


	public void startSubscribe() {
		try {
			MqttClient sampleClient = new MqttClient(broker, clientId,
					persistence);
			sampleClient.setCallback(new MqttCallback() {

				@Override
				public void messageArrived(String topic, MqttMessage msg)
						throws Exception {
					logger.info("topic:" + topic);
					logger.info("msg:" + new String(msg.getPayload()));

						sendData(topic, new String(msg.getPayload()));

				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
					logger.info("delivary complete");
				}

				@Override
				public void connectionLost(Throwable cause) {
					try
					{
			    		MqttClient sampleClient = new MqttClient(broker, clientId,
								persistence);
				    	// Connect to the server
			    	
					
					boolean connected = true;
					logger.info("Connection to " + broker + " lost!");
					do
					{
						connected = true;
						logger.info("Re - Connecting to "+broker);
				    	try 
				    	{
					    	// Connect to the server
				    		sampleClient.connect();
				    	}
				    	catch (MqttException e) 
				    	{
				    		connected = false;
				    		logger.info("Unable to connect: "+e.toString());
							try {
								Thread.sleep(5000L);    // one second
							}catch (Exception sleep) {}  

						}
					}while(!connected);
					logger.info("Connected to " + broker);
					logger.info("Connected !!!");
			    	// Subscribe to the topic
					logger.info("Subscribing to topic \""+topic+"\" qos "+qos);
			    	try {
			    		sampleClient.subscribe(topic, qos);
					} catch (MqttSecurityException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.info("Unable to subscribe: "+e.toString());
					} catch (MqttException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.info("Unable to subscribe: "+e.toString());
					}
			    	logger.info("Subscribed to topic \""+topic+"\" qos "+qos);
					}
			    	catch (MqttException e) 
			    	{
			    		logger.info("Unable to connect: "+e.toString());
						try {
							Thread.sleep(5000L);    // one second
						}catch (Exception sleep) {}  

					}
				
				}
					
			});

			MqttConnectOptions options = new MqttConnectOptions();
			options.setUserName("admin");
			options.setPassword(new char[] { 'p', 'a', 's', 's', 'w', 'o', 'r',
					'd' });
			sampleClient.connect(options);
			sampleClient.subscribe(topic, qos);
			logger.info("Subscribing Started");
		} catch (MqttException me) {
			logger.info("reason " + me.getReasonCode());
			logger.info("msg " + me.getMessage());
			logger.info("loc " + me.getLocalizedMessage());
			logger.info("cause " + me.getCause());
			logger.info("excep " + me);
			me.printStackTrace();
		}
	}
	
	
	private static void sendData(String sensorname,String value) {
		try {
			logger.info("topic:" + sensorname);
			logger.info("msg:" + value);
			sensorname = sensorname.replace("data/", "");
			sensorname = sensorname.replace("/", "_");
			logger.info("sensorName=" + sensorname);

			long epoch = System.currentTimeMillis() / 1000;

			
			String[] type=sensorname.split("_");
			
			String js="{"
					+ "\n\"timed\":\""+epoch+"\","
					+ "\n\""+type[1]+"\":\""+value+"\""
					+ "\n}";

			logger.info(js);

			FileWriter fileWriter = new FileWriter("/tmp/" + sensorname + ".txt");
			fileWriter.write(js);
			fileWriter.flush();
			
			
			
			String urlToConnect = "http://52.0.88.104:22001/gadgeon/iot/GadgeonTemperatureSensor1/"
					+ sensorname + "/reportEvent";
			File fileToUpload = new File("/tmp/" + sensorname + ".txt");
			String boundary = Long.toHexString(System.currentTimeMillis());
			URLConnection connection = new URL(urlToConnect).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + boundary);
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new OutputStreamWriter(
						connection.getOutputStream(), "UTF-8"));
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(fileToUpload), "UTF-8"));
					for (String line; (line = reader.readLine()) != null;) {
						writer.println(line);
					}
				} finally {
					if (reader != null)
						try {
							reader.close();
						} catch (IOException logOrIgnore) {
							//
						}
				}
			} finally {
				if (writer != null)
					writer.close();
			}
			int responseCode = ((HttpURLConnection) connection)
					.getResponseCode();
			if (responseCode == 200) {
				logger.info("success");
			} else {
				logger.info("fail");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}