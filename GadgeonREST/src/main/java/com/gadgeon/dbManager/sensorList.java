package com.gadgeon.dbManager;

public class sensorList {

	private String sensorid;
	private String sensortype;
	private String sensorlocation;
	private Double sensoralarmcnt;
	private String lastuptime;
	private String sensorstatus;
	private String latitude;
	private String longitude;

	public sensorList(String sensorid, String sensortype,
			String sensorlocation, Double sensoralarmcnt, String lastuptime,
			String sensorstatus,String latitude,String longitude) {
		this.setSensorid(sensorid);
		this.setSensortype(sensortype);
		this.setSensorlocation(sensorlocation);
		this.setSensoralarmcnt(sensoralarmcnt);
		this.setLastuptime(lastuptime);
		this.setSensorstatus(sensorstatus);
		this.setLatitude(latitude);
		this.setLongitude(longitude);
	}

	public String getSensorid() {
		return sensorid;
	}

	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}

	public String getSensortype() {
		return sensortype;
	}

	public void setSensortype(String sensortype) {
		this.sensortype = sensortype;
	}

	public Double getSensoralarmcnt() {
		return sensoralarmcnt;
	}

	public void setSensoralarmcnt(Double sensoralarmcnt) {
		this.sensoralarmcnt = sensoralarmcnt;
	}

	public String getSensorlocation() {
		return sensorlocation;
	}

	public void setSensorlocation(String sensorlocation) {
		this.sensorlocation = sensorlocation;
	}

	public String getLastuptime() {
		return lastuptime;
	}

	public void setLastuptime(String lastuptime) {
		this.lastuptime = lastuptime;
	}

	public String getSensorstatus() {
		return sensorstatus;
	}

	public void setSensorstatus(String sensorstatus) {
		this.sensorstatus = sensorstatus;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
