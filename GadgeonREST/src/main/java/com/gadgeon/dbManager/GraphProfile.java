package com.gadgeon.dbManager;

public class GraphProfile {
	private String graphprofile;
	private String sensortype;
	private String xlabel;
	private String ylabel;
	private Double min;
	private Double max;
	
	
	
	
	public GraphProfile( String graphprofile,String sensortype,String xlabel,String ylabel,Double min,Double max)
	{
		this.setGraphprofile(graphprofile);
		this.setSensortype(sensortype);
		this.setXlabel(xlabel);
		this.setYlabel(ylabel);
		this.setMin(min);
		this.setMax(max);
		
	}
	public String getGraphprofile() {
		return graphprofile;
	}
	public void setGraphprofile(String graphprofile) {
		this.graphprofile = graphprofile;
	}
	public String getSensortype() {
		return sensortype;
	}
	public void setSensortype(String sensortype) {
		this.sensortype = sensortype;
	}
	public String getXlabel() {
		return xlabel;
	}
	public void setXlabel(String xlabel) {
		this.xlabel = xlabel;
	}
	public String getYlabel() {
		return ylabel;
	}
	public void setYlabel(String ylabel) {
		this.ylabel = ylabel;
	}
	public Double getMin() {
		return min;
	}
	public void setMin(Double min) {
		this.min = min;
	}
	public Double getMax() {
		return max;
	}
	public void setMax(Double max) {
		this.max = max;
	}


}
