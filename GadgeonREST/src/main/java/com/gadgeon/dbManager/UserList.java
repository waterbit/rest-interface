package com.gadgeon.dbManager;

public class UserList {

		private String userid;
		private String username;
		private String userInfo;
		private String email;
		private String phone;
	
	 
		
		public UserList() {
			// TODO Auto-generated constructor stub
		}
		
		public UserList(String userid, String username,String userInfo,String email,String phone) {
	    this.setUserid(userid);
	    this.setUsername(username);
	    this.setUserInfo(userInfo);
	    this.setEmail(email);
	    this.setPhone(phone);
	  }

		public String getUserid() {
			return userid;
		}

		public void setUserid(String userid) {
			this.userid = userid;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getUserInfo() {
			return userInfo;
		}

		public void setUserInfo(String userInfo) {
			this.userInfo = userInfo;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}
}
