package com.gadgeon.dbManager;

public class SensorGraph {
	private String sensorname;
	private String profilename;
	
	
	public SensorGraph(String sensorname, String profilename) {
		this.sensorname = sensorname;
		this.profilename = profilename;
	}
	public String getSensorname() {
		return sensorname;
	}
	public void setSensorname(String sensorname) {
		this.sensorname = sensorname;
	}
	public String getProfilename() {
		return profilename;
	}
	public void setProfilename(String profilename) {
		this.profilename = profilename;
	}

}
