package com.gadgeon.dbManager;

public class SensorData {
	private String sensorid;
	private String sensortype;
	private String sensorlocation;
	private String sensorinfo;
	private String max;
	private String min;
	private String latitude;
	private String longitude;
	
	
	public SensorData(String sensorid, String sensortype,
			String sensorlocation, String sensorinfo, String max, String min,String latitude, String longitude) {
		this.sensorid = sensorid;
		this.sensortype = sensortype;
		this.sensorlocation = sensorlocation;
		this.sensorinfo = sensorinfo;
		this.max = max;
		this.min = min;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	
	public String getSensorid() {
		return sensorid;
	}
	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}
	public String getSensortype() {
		return sensortype;
	}
	public void setSensortype(String sensortype) {
		this.sensortype = sensortype;
	}
	public String getSensorlocation() {
		return sensorlocation;
	}
	public void setSensorlocation(String sensorlocation) {
		this.sensorlocation = sensorlocation;
	}
	public String getSensorinfo() {
		return sensorinfo;
	}
	public void setSensorinfo(String sensorinfo) {
		this.sensorinfo = sensorinfo;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
