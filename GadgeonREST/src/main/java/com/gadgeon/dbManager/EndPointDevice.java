package com.gadgeon.dbManager;

public class EndPointDevice {
	String enddeviceid;
	String enddeviceinfo;
	String latitude;
	String longitude;
	String location;
	String timeintervel;

	public EndPointDevice(String enddeviceid, String enddeviceinfo,
			String latitude, String longitude, String location,
			String timeintervel) {

		this.enddeviceid = enddeviceid;
		this.enddeviceinfo = enddeviceinfo;
		this.latitude = latitude;
		this.longitude = longitude;
		this.location = location;
		this.timeintervel = timeintervel;
	}

	public String getEnddeviceid() {
		return enddeviceid;
	}

	public void setEnddeviceid(String enddeviceid) {
		this.enddeviceid = enddeviceid;
	}

	public String getEnddeviceinfo() {
		return enddeviceinfo;
	}

	public void setEnddeviceinfo(String enddeviceinfo) {
		this.enddeviceinfo = enddeviceinfo;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTimeintervel() {
		return timeintervel;
	}

	public void setTimeintervel(String timeintervel) {
		this.timeintervel = timeintervel;
	}
}
