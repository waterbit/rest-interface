package com.gadgeon.dbManager;

public class SensorListByEndDevice {
	String sensorname;

	public String getSensorname() {
		return sensorname;
	}

	public void setSensorname(String sensorname) {
		this.sensorname = sensorname;
	}

	public SensorListByEndDevice(String sensorname) {
		this.sensorname = sensorname;
	}
}
