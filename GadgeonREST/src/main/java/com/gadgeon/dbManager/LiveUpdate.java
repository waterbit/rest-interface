package com.gadgeon.dbManager;



import java.util.HashMap;
import java.util.Map;

import com.gadgeon.email.EmailService;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.sensormetadata;




public class LiveUpdate implements Runnable{
	
	EmailService es = new EmailService();
	dbOperation dbo = new dbOperation();
	
	private String sensorid;
	private Double value;
	
	public String getSensorid() {
		return sensorid;
	}

	public void setSensorid(String sensorid) {
		this.sensorid = sensorid;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}	 
	
	
	public LiveUpdate(){
        
    }
    
	public LiveUpdate(String sensorid, Double value){
		this.setSensorid(sensorid);
		this.setValue(value);
	
	
    }
	
    public void run()  {
      
            try {
				
            	updateCurrentVal(getSensorid(),getValue());
            	
			} catch (Exception e) {
				
			}
    } 
    
    public void updateCurrentVal(String sensorid,Double val) throws Exception
	{
		EmailService es = new EmailService();
		 Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
		loadDataMap mp =new loadDataMap();
		sensormap=mp.getSensormap();
		sensormetadata ds = new sensormetadata();
		ds=sensormap.get(sensorid);
		dbOperation dbo= new dbOperation();
		Double min=ds.getSensormin();
		Double max=ds.getSensormax();
		
			if(val<min)
			{
				ds.setSensoralarmcnt(ds.getSensoralarmcnt()+1);
				dbo.updateAlarmcount(ds);
				/*es._message="Current Sensor Value="+val.toString()+" ( "+sensorid+" Cross Minimum Threshold )";
				Thread t = new Thread(es);
		        t.start();*/
			}
			if(val>max)
			{
				ds.setSensoralarmcnt(ds.getSensoralarmcnt()+1);
				dbo.updateAlarmcount(ds);
				/*es._message="Current Sensor Value="+val.toString()+" ( "+sensorid+" Cross Maximum Threshold )";
				Thread t = new Thread(es);
		        t.start();*/
			}
			ds.setSensorcrntval(val);
		//	dbo.updateCurrent(ds);
			
			


	}

	

}
