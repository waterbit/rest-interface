package com.gadgeon.dbManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gadgeon.dataRetrieval.retrievalManager;
import com.gadgeon.initMap.loadDataMap;
import com.gadgeon.sensor.SensorDelete;
import com.gadgeon.sensor.sensormetadata;
import com.mysql.jdbc.Util;

import org.apache.log4j.Logger;

import java.util.StringTokenizer;

/**
 * Class is used to perform database operations
 * 
 * @function insert : A function to insert data to database
 * @function update : A function to update data in database
 * @function getSensorList : A function to read sensor ids from database
 * @function getAlarmCount : A function to read alarm count from database
 * @function getCurrentValue : A function to read current value from database
 * @function clearAlarm : A function is used to clear the alarm count in
 *           database
 */

public class dbOperation {

	static List<String> sensorlist = new ArrayList<String>();
	static String dbname = "GadgeonSensorSchema";
	final static Logger logger = Logger.getLogger(dbOperation.class);
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();
	public Map<String, sensormetadata> sensormap = new HashMap<String, sensormetadata>();
	public loadDataMap mp = new loadDataMap();

	/**
	 * Function sensorRegister is used to insert sensor data to mysql database
	 * 
	 * @param mdata
	 *            : object of class SensorMetaData, it contain complete details
	 *            about sensor
	 */

	public boolean sensorRegister(sensormetadata mdata) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> sensorRegister\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			preparedStatement = conn
					.prepareStatement("INSERT INTO "
							+ tbname
							+ " (`sensorid`, `sensortype`, `sensorinfo`, `sensorunit`, `sensormin`, `sensormax`, `sensorlati`, `sensorlongi`, `sensoralarmcnt`, `sensorcrntval`, `sensorstatus`, `notify`, `userid`, `location`, `lastuptime`, `enddevice`) VALUES (?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ?,?,?,?);");
			preparedStatement.setString(1, mdata.getSensorid());
			preparedStatement.setString(2, mdata.getSensortype());
			preparedStatement.setString(3, mdata.getSensorinfo());
			preparedStatement.setString(4, mdata.getSensorunit());
			preparedStatement.setDouble(5, mdata.getSensormin());
			preparedStatement.setDouble(6, mdata.getSensormax());
			preparedStatement.setString(7, mdata.getSensorlati());
			preparedStatement.setString(8, mdata.getSensorlongi());
			preparedStatement.setDouble(9, 0);
			preparedStatement.setDouble(10, 0);
			preparedStatement.setString(11, "normal");
			preparedStatement.setInt(12, 0);
			preparedStatement.setString(13, mdata.getUserid());
			preparedStatement.setString(14, mdata.getLocation());
			preparedStatement.setString(15, mdata.getLastuptime());
			preparedStatement.setString(16, mdata.getEnddevice());
			preparedStatement.execute();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor registered successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return false;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return true;
	}

	/**
	 * Function is used to list sensor ids from database
	 * 
	 * @return sensorlist : A string array list contains all sensor ids
	 * @throws SQLException
	 */

	public static List<String> getSensorList() {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorList\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			Statement st = (Statement) conn.createStatement();
			String query = "select sensorid from " + tbname;
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				sensorlist.add(rs.getString("sensorid"));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor list returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return sensorlist;
	}

	public sensormetadata loadData(String sensorid, sensormetadata da) {

		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> loadData\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			Statement st = conn.createStatement();
			String sql1 = "select * from " + tbname + " where sensorid='"
					+ sensorid + "'";
			ResultSet rs = st.executeQuery(sql1);
			if (rs.next()) {
				da.setSensortype(rs.getString("sensortype"));
				da.setSensorid(rs.getString("sensorid"));
				da.setSensorinfo(rs.getString("sensorinfo"));
				da.setSensorunit(rs.getString("sensorunit"));
				da.setSensormin(rs.getDouble("sensormin"));
				da.setSensormax(rs.getDouble("sensormax"));
				da.setSensorlati(rs.getString("sensorlati"));
				da.setSensorlongi(rs.getString("sensorlongi"));
				da.setSensoralarmcnt(rs.getDouble("sensoralarmcnt"));
				da.setSensorcrntval(rs.getDouble("sensorcrntval"));
				da.setSensorstatus(rs.getString("sensorstatus"));
				da.setLocation(rs.getString("location"));
				da.setNotify(rs.getInt("notify"));
				da.setUserid(rs.getString("userid"));
				da.setLastuptime(rs.getString("lastuptime"));
				da.setEnddevice(rs.getString("enddevice"));
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": Sensor data loading completed");
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return da;
	}

	/**
	 * Function used to update data in database
	 * 
	 * @param sensorid
	 *            : A string data containing sensor id to check
	 * @param crntval
	 *            : A Double value containing current value to update
	 * @throws SQLException
	 */

	public void updateCurrent(sensormetadata md) {

		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateCurrent\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");

			String tbname = dbname + ".sensormetadata";

			String query = "update "
					+ tbname
					+ " set sensorcrntval = ? where sensorid = ? and userid = ?";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setDouble(1, md.getSensorcrntval());
			preparedStatement.setString(2, md.getSensorid());
			preparedStatement.setString(3, md.getUserid());
			preparedStatement.execute();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": updated current value successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	/**
	 * Function is used to clear the alarm count in database
	 * 
	 * @param sensorid
	 *            : A string containing sensor id to check
	 * @throws SQLException
	 */

	public void clearAlarm(String sensorid) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> clearAlarm\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "update " + tbname
					+ " set sensoralarmcnt = ? where sensorid = ?";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setDouble(1, 0);
			preparedStatement.setString(2, sensorid);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm cleared successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	/**
	 * Function is used to validate the login details
	 * 
	 * @param username
	 *            : A string data containing user name to check
	 * @param password
	 *            : A string data containing password to check
	 * @return userid : An integer value returning the userid of the
	 *         curresponding user
	 * @throws SQLException
	 */

	public String userLogin(String username, String password) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int userId = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> userLogin\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			String query = "SELECT userid FROM " + tbname + " where username='"
					+ username + "' and password='" + password + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				userId = rs.getInt("userid");
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user login successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

		return Integer.toString(userId);
	}

	/**
	 * Function is used to insert data to userDetails
	 * 
	 * @param mdata
	 *            : A string list containing data to insert
	 * @throws Exception
	 */

	public void userRegister(String username, String password, String userInfo,
			String email, String phone) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int user = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> userRegister\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			user = validateUser(username, password);
			if (user == 0) {
				String tbname = dbname + ".userdetails";
				preparedStatement = conn
						.prepareStatement("INSERT INTO "
								+ tbname
								+ " (`userid`, `username`, `password`, `userInfo`, `email`, `phone`) VALUES (?, ?, ?, ?, ?, ?);");
				preparedStatement.setInt(1, 0);
				preparedStatement.setString(2, username);
				preparedStatement.setString(3, password);
				preparedStatement.setString(4, userInfo);
				preparedStatement.setString(5, email);
				preparedStatement.setString(6, phone);
				preparedStatement.execute();

				logger.info(loggerdateFormat.format(loggerdate)
						+ ": user registred successfully\n");
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	private int validateUser(String username, String password) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int user = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> validateUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			String query = "SELECT userid FROM " + tbname + " where username='"
					+ username + "' and password='" + password + "'";

			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				user = rs.getInt("userid");
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user validation successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return user;
	}

	public int changePassword(String userid, String oldpass, String newpass) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> changePassword\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			String pwd = null;
			Statement st = (Statement) conn.createStatement();
			String query1 = "select password from " + tbname + " where userid="
					+ Integer.parseInt(userid);
			ResultSet rs = st.executeQuery(query1);
			while (rs.next()) {
				pwd = rs.getString("password");
			}
			if (pwd.equals(oldpass)) {
				String query = "update " + tbname
						+ " set password = ? where userid = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, newpass);
				preparedStatement.setInt(2, Integer.parseInt(userid));
				preparedStatement.executeUpdate();

			} else {
				throw new Exception();
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": password changed successfully\n");
			return 1;
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return 0;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	public ArrayList<UserList> getUserList() {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<UserList> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getUserList\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " WHERE `userid` != 1";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<UserList>();
			while (rs.next()) {
				mylist.add(new UserList(rs.getString("userid"), rs
						.getString("username"), rs.getString("userInfo"), rs
						.getString("email"), rs.getString("phone")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user list returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public void deleteUser(int userid) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			String tbname1 = dbname + ".sensormetadata";
			retrievalManager rm = new retrievalManager();
			int user = 0;
			Statement st = (Statement) conn.createStatement();
			String query1 = "select userid from " + tbname + " where userid="
					+ userid;
			ResultSet rs = st.executeQuery(query1);
			while (rs.next()) {
				user = rs.getInt("userid");
			}
			if (user != 0) {
				String query = "delete from " + tbname + " where userid = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setInt(1, userid);
				preparedStatement.executeUpdate();
				String query2 = "select sensorid from " + tbname1
						+ " where userid=" + userid;
				ResultSet rs2 = st.executeQuery(query2);
				String sensr = "";
				while (rs2.next()) {
					sensr = rs2.getString("sensorid");
				}
				if (sensr != "") {
					String query3 = "delete from " + tbname1
							+ " where userid = ?";
					preparedStatement = conn.prepareStatement(query3);
					preparedStatement.setInt(1, userid);
					preparedStatement.executeUpdate();
					rm.deleteSensor(userid);
				}
			} else {
				throw new Exception();
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user deletion successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	public void updateUser(int userid, String userInfo, String email,
			String phone) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".userdetails";
			int usr = 0;
			Statement st = (Statement) conn.createStatement();
			String query1 = "select userid from " + tbname + " where userid="
					+ userid;
			ResultSet rs = st.executeQuery(query1);
			while (rs.next()) {
				usr = rs.getInt("userid");
			}

			if (usr != 0) {
				String query = "update "
						+ tbname
						+ " set userInfo = ? , email = ? , phone = ? where userid = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, userInfo);
				preparedStatement.setString(2, email);
				preparedStatement.setString(3, phone);
				preparedStatement.setInt(4, userid);
				preparedStatement.executeUpdate();

			} else {
				throw new SQLException();
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": updated user successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public void editSensor(sensormetadata md) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> editSensor\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "update "
					+ tbname
					+ " set sensormax = ? , sensormin = ? ,sensorlati = ? , sensorlongi = ?, location= ?, sensorinfo = ?  where sensorid='"
					+ md.getSensorid() + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setDouble(1, md.getSensormax());
			preparedStatement.setDouble(2, md.getSensormin());
			preparedStatement.setString(3, md.getSensorlati());
			preparedStatement.setString(4, md.getSensorlongi());
			preparedStatement.setString(5, md.getLocation());
			preparedStatement.setString(6, md.getSensorinfo());
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor edited successfully\n");

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public void deleteSensor(String sensorid) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteSensor\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "delete from " + tbname + " where sensorid='"
					+ sensorid + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor deleted successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	public void updateAlarmcount(sensormetadata ds) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateAlarmcount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");

			String tbname = dbname + ".sensormetadata";
			String query = "update "
					+ tbname
					+ " set sensoralarmcnt = ? where sensorid = ? and userid = ?";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setDouble(1, ds.getSensoralarmcnt());
			preparedStatement.setString(2, ds.getSensorid());
			preparedStatement.setString(3, ds.getUserid());
			preparedStatement.executeUpdate();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm count updated  successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	public void updateLastTime(sensormetadata ds) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateLastTime\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");

			String tbname = dbname + ".sensormetadata";
			String query = "update " + tbname
					+ " set lastuptime = ? where sensorid = ? and userid = ?";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, ds.getLastuptime());
			preparedStatement.setString(2, ds.getSensorid());
			preparedStatement.setString(3, ds.getUserid());
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": last uploaded time updated  successfully\n");

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public int addsensortype(String sensortype) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addsensortype\n");

			count = validatesensortype(sensortype);
			if (count == 0) {

				Class.forName("com.mysql.jdbc.Driver").newInstance();

				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/" + "GadgeonSensorSchema",
						"gadgeon", "gadgeon123");

				String tbname = dbname + ".sensortype";
				preparedStatement = conn.prepareStatement("INSERT INTO "
						+ tbname + " (`sensortype`) VALUES (?);");
				preparedStatement.setString(1, sensortype);
				preparedStatement.execute();
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": sensor type added  successfully\n");
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return 0;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	private int validatesensortype(String sensortype) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> validateUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensortype";
			String query = "SELECT id FROM " + tbname + " where sensortype='"
					+ sensortype + "'";

			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt("id");
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user validation successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public ArrayList<String> getsensortype() {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<String> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getsensortype\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");

			String tbname = dbname + ".sensortype";
			Statement st = (Statement) conn.createStatement();
			String query = "SELECT * FROM " + tbname;
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<String>();
			while (rs.next()) {
				mylist.add(rs.getString("sensortype"));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor type returned  successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;

	}

	public int addgraphprofile(String profileName, String sensorType,
			String xLabel, String yLabel, double min, double max) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addgraphprofile\n");

			count = validategraphprofile(profileName);
			if (count == 0) {

				Class.forName("com.mysql.jdbc.Driver").newInstance();

				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/" + "GadgeonSensorSchema",
						"gadgeon", "gadgeon123");

				String tbname = dbname + ".graphprofile";
				preparedStatement = conn
						.prepareStatement("INSERT INTO "
								+ tbname
								+ " (`profilename`,`sensortype`,`xlabel`,`ylabel`,`min`,`max`) VALUES (?,?,?,?,?,?);");
				preparedStatement.setString(1, profileName);
				preparedStatement.setString(2, sensorType);
				preparedStatement.setString(3, xLabel);
				preparedStatement.setString(4, yLabel);
				preparedStatement.setDouble(5, min);
				preparedStatement.setDouble(6, max);
				preparedStatement.setEscapeProcessing(false);
				preparedStatement.execute();

				logger.info(loggerdateFormat.format(loggerdate)
						+ ": graph profile inserted  successfully\n");
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return 0;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	private int validategraphprofile(String profileName) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> validateUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			String query = "SELECT id FROM " + tbname + " where profilename='"
					+ profileName + "'";

			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt("id");
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user validation successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public ArrayList<GraphProfile> getGraphProfile() {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<GraphProfile> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getGraphProfile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname
					+ " where profilename != 'default' ";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<GraphProfile>();
			while (rs.next()) {
				mylist.add(new GraphProfile(rs.getString("profilename"), rs
						.getString("sensortype"), rs.getString("xlabel"), rs
						.getString("ylabel"), rs.getDouble("min"), rs
						.getDouble("max")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public void deletesensortype(String sensortype) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deletesensortype\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensortype";
			String query = "delete from " + tbname + " where sensortype='"
					+ sensortype + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.executeUpdate();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor type deleted  successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	public void editSensorProfile(String sensorname, String profile)
			throws Exception {
		Connection conn = null;
		PreparedStatement preparedStatement = null;

		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> editSensorProfile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensor_graph";
			String query = "update " + tbname
					+ " set profile = ?  where sensor='" + sensorname + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, profile);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor graph profile updated  successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public void deleteGraphprofile(String profilename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteGraphprofile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			String query = "delete from " + tbname + " where profilename='"
					+ profilename + "'";

			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setEscapeProcessing(false);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ":graph profile deleted  successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public ArrayList<GraphProfile> getGraphProfilebytype(String sensortype) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<GraphProfile> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getGraphProfilebytype\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where sensortype='"
					+ sensortype + "'";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<GraphProfile>();
			while (rs.next()) {
				mylist.add(new GraphProfile(rs.getString("profilename"), rs
						.getString("sensortype"), rs.getString("xlabel"), rs
						.getString("ylabel"), rs.getDouble("min"), rs
						.getDouble("max")));
			}

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned using sensor type successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;

	}

	public ArrayList<GraphProfile> getGraphProfilebyname(String profilename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<GraphProfile> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getGraphProfilebyname\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where profilename='"
					+ profilename + "'";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<GraphProfile>();
			while (rs.next()) {
				mylist.add(new GraphProfile(rs.getString("profilename"), rs
						.getString("sensortype"), rs.getString("xlabel"), rs
						.getString("ylabel"), rs.getDouble("min"), rs
						.getDouble("max")));
			}

			if (mylist.isEmpty()) {
				if (profilename == "NA") {
					mylist.add(new GraphProfile("NA", "NA", "NA", "NA", 0.0,
							0.0));
				}
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned using profile name successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public String check(String profilename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		String result = null;
		try {

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> check\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where profilename='"
					+ profilename + "'";
			ResultSet rs = st.executeQuery(query);
			if (!rs.isBeforeFirst()) {
				result = "no";
			} else {
				result = "yes";
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile checked successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return result;
	}

	public ArrayList<SensorGraph> getsensorgraph(String sensorname) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<SensorGraph> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getsensorgraph\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensor_graph";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where sensor= '"
					+ sensorname + "'";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<SensorGraph>();
			while (rs.next()) {
				mylist.add(new SensorGraph(rs.getString("sensor"), rs
						.getString("profile")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor graph returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public void addSensorGraphProfile(String sensorname, String profilename) {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addSensorGraphProfile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensor_graph";
			preparedStatement = conn.prepareStatement("INSERT INTO " + tbname
					+ " (`sensor`, `profile`) VALUES (?, ?);");
			preparedStatement.setString(1, sensorname);
			preparedStatement.setString(2, profilename);
			preparedStatement.execute();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensorgraph returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public void editGraphProfile(String profile, String xlabel, String ylabel,
			String min, String max) throws Exception {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> editGraphProfile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			String query = "update "
					+ tbname
					+ " set xlabel = ?, ylabel = ? , min = ?, max = ? where profilename='"
					+ profile + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, xlabel);
			preparedStatement.setString(2, ylabel);
			preparedStatement.setDouble(3, Double.parseDouble(min));
			preparedStatement.setDouble(4, Double.parseDouble(max));
			preparedStatement.setEscapeProcessing(false);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile updated successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + e.getMessage()
					+ "\n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public void deleteSensorProfile(String sensorname) {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteSensorProfile\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensor_graph";
			String query = "delete from " + tbname + " where sensor='"
					+ sensorname + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor profile deleted successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public int getsensorcount(String sensortype) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getsensorcount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "SELECT count( * ) FROM " + tbname
					+ " where sensortype='" + sensortype + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor count returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public int getprofilecount(String sensortype) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getprofilecount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".graphprofile";
			String query = "SELECT count( * ) FROM " + tbname
					+ " where sensortype='" + sensortype + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profilecount returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

		return count;
	}

	public int sensorgraphcount(String profilename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> sensorgraphcount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensor_graph";
			String query = "SELECT count( * ) FROM " + tbname
					+ " where profile='" + profilename + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor graph count returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public void updateAlarmStatus(String sensorid, String status) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> updateAlarmStatus\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "update " + tbname
					+ " set sensorstatus = ? where sensorid = ?";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, status);
			preparedStatement.setString(2, sensorid);
			preparedStatement.executeUpdate();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": alarm status updated successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public int addEndDevice(String enddeviceid, String enddeviceinfo,
			String latitude, String longitude, String location,
			String timeintervel) {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> addEndDevice\n");

			count = validateenddevice(enddeviceid);
			if (count == 0) {

				Class.forName("com.mysql.jdbc.Driver").newInstance();
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/" + "GadgeonSensorSchema",
						"gadgeon", "gadgeon123");
				String tbname = dbname + ".EndPointDevice";
				preparedStatement = conn
						.prepareStatement("INSERT INTO "
								+ tbname
								+ " (`enddeviceid`,`enddeviceinfo`, `latitude`, `longitude`, `location`, `timeintervel`) VALUES (?, ?, ?, ?, ?, ?);");
				preparedStatement.setString(1, enddeviceid);
				preparedStatement.setString(2, enddeviceinfo);
				preparedStatement.setString(3, latitude);
				preparedStatement.setString(4, longitude);
				preparedStatement.setString(5, location);
				preparedStatement.setString(6, timeintervel);
				preparedStatement.execute();
				logger.info(loggerdateFormat.format(loggerdate)
						+ ": addEndDevice returned successfully\n");
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return 0;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	private int validateenddevice(String enddeviceid) {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> validateUser\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".EndPointDevice";
			String query = "SELECT id FROM " + tbname + " where enddeviceid='"
					+ enddeviceid + "'";

			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt("id");
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": user validation successfull\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public ArrayList<EndPointDevice> getEndPointDevice() {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<EndPointDevice> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getEndPointDevice\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".EndPointDevice";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname;
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<EndPointDevice>();
			while (rs.next()) {
				mylist.add(new EndPointDevice(rs.getString("enddeviceid"), rs
						.getString("enddeviceinfo"), rs.getString("latitude"),
						rs.getString("longitude"), rs.getString("location"), rs
								.getString("timeintervel")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public void deleteEndPoint(String endpointdevicename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> deleteSensor\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".EndPointDevice";
			String query = "delete from " + tbname + " where enddeviceid='"
					+ endpointdevicename + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.executeUpdate();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor deleted successfully\n");

			int sensorcount = getSensorCount(endpointdevicename);
			List<String> namelist = new ArrayList<>();
			if (sensorcount != 0) {
				namelist = getSensorNameByEnddevice(endpointdevicename);
				for (String name : namelist) {
					Thread t = new Thread(new SensorDelete(name));
					t.start();
					sensormap = mp.getSensormap();
					sensormap.remove(name);
					deleteSensorProfile(name);
					Thread.sleep(800);
				}
			}

		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
	}

	private List<String> getSensorNameByEnddevice(String endpointdevicename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorCount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "SELECT *  FROM " + tbname + " where enddevice='"
					+ endpointdevicename + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			List<String> namelist = new ArrayList<>();
			while (rs.next()) {
				namelist.add(rs.getString("sensorid"));
			}
			st.close();

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor count returned successfully\n");
			return namelist;
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
			return null;
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	private int getSensorCount(String endpointdevicename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		int count = 0;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getSensorCount\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			String query = "SELECT count( * ) FROM " + tbname
					+ " where enddevice='" + endpointdevicename + "'";
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			st.close();
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": sensor count returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return count;
	}

	public ArrayList<EndPointDevice> getEndPointDeviceByName(String devicename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<EndPointDevice> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getEndPointDevice\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".EndPointDevice";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where enddeviceid= '"
					+ devicename + "'";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<EndPointDevice>();
			while (rs.next()) {
				mylist.add(new EndPointDevice(rs.getString("enddeviceid"), rs
						.getString("enddeviceinfo"), rs.getString("latitude"),
						rs.getString("longitude"), rs.getString("location"), rs
								.getString("timeintervel")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

	public void editEndDevice(String enddeviceid, String deviceinfo,
			String latitude, String longitude, String location,
			String timeintervel) throws Exception {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> editEndDevice\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".EndPointDevice";
			String query = "update "
					+ tbname
					+ " set enddeviceinfo = ?, latitude = ? , longitude = ?, location = ? , timeintervel = ? where enddeviceid='"
					+ enddeviceid + "'";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, deviceinfo);
			preparedStatement.setString(2, latitude);
			preparedStatement.setString(3, longitude);
			preparedStatement.setString(4, location);
			preparedStatement.setString(5, timeintervel);
			preparedStatement.executeUpdate();

			int sensorcount = getSensorCount(enddeviceid);
			List<String> namelist = new ArrayList<>();
			if (sensorcount != 0) {
				namelist = getSensorNameByEnddevice(enddeviceid);
				sensormap = mp.getSensormap();
				for (String name : namelist) {
					sensormetadata value = sensormap.get(name);
					if (value != null) {
						value.setLocation(location);
						value.setSensorlati(latitude);
						value.setSensorlongi(longitude);
						editSensor(value);
					}
					Thread.sleep(200);
				}
			}

			logger.info(loggerdateFormat.format(loggerdate)
					+ ": EndDevice updated successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + e.getMessage()
					+ "\n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}

	}

	public ArrayList<SensorListByEndDevice> getSensorNameByEndPointDevice(
			String devicename) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		ArrayList<SensorListByEndDevice> mylist = null;
		try {
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getEndPointDevice\n");

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"
					+ "GadgeonSensorSchema", "gadgeon", "gadgeon123");
			String tbname = dbname + ".sensormetadata";
			Statement st = (Statement) conn.createStatement();
			String query = "select * from " + tbname + " where enddevice= '"
					+ devicename + "'";
			ResultSet rs = st.executeQuery(query);
			mylist = new ArrayList<SensorListByEndDevice>();
			while (rs.next()) {
				mylist.add(new SensorListByEndDevice(rs.getString("sensorid")));
			}
			logger.info(loggerdateFormat.format(loggerdate)
					+ ": graph profile returned successfully\n");
		} catch (Exception e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) { /* ignored */
					logger.error(loggerdateFormat.format(loggerdate) + ": "
							+ e.getMessage() + " \n");
				}
			}
		}
		return mylist;
	}

}