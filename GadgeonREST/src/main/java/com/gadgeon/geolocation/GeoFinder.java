package com.gadgeon.geolocation;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.gadgeon.email.EmailService;


public class GeoFinder {

	final static Logger logger = Logger.getLogger(EmailService.class);
	public static SimpleDateFormat loggerdateFormat = new SimpleDateFormat(
			"yyyy MMM dd HH:mm:ss");
	public static Date loggerdate = new Date();
	
	public  JSONObject getLocationInfo( String lat, String lng) {

	    HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false");
	    HttpClient client = new DefaultHttpClient();
	    HttpResponse response;
	    StringBuilder stringBuilder = new StringBuilder();

	    try {
	    	logger.info(loggerdateFormat.format(loggerdate)
					+ ": Control on method --> getLocationInfo\n");
	    	
	        response = client.execute(httpGet);
	        HttpEntity entity = response.getEntity();
	        InputStream stream = entity.getContent();
	        int b;
	        while ((b = stream.read()) != -1) {
	            stringBuilder.append((char) b);
	        }
	    } catch (ClientProtocolException e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
	        } catch (IOException e) {
	    		logger.error(loggerdateFormat.format(loggerdate) + ": "
						+ e.getMessage() + " \n");
	    }

	    JSONObject jsonObject = new JSONObject();
	    try {
	        jsonObject = new JSONObject(stringBuilder.toString());
	    } catch (JSONException e) {
			logger.error(loggerdateFormat.format(loggerdate) + ": "
					+ e.getMessage() + " \n");
	        e.printStackTrace();
	    }
	    
	    logger.info(loggerdateFormat.format(loggerdate)
				+ ": location info returned successfully\n");
	    
	    return jsonObject;
	}
	
	
}